
CREATE DATABASE IF NOT EXISTS mnemosyne CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE USER 'mnemosyne'@'%' IDENTIFIED BY 'mnemosyne';
GRANT ALL PRIVILEGES ON mnemosyne.* TO 'mnemosyne'@'%';

flush privileges;
