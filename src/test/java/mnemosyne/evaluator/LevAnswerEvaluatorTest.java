package mnemosyne.evaluator;

import mnemosyne.evaluator.impl.LevAnswerEvaluator;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class LevAnswerEvaluatorTest {
    @Test
    public void expectCorrectGuess() {
        IAnswerEvaluator eval = new LevAnswerEvaluator();
        String givenAnswer = "soeren";
        Set<String> acceptableAnswers = new HashSet<>(Arrays.asList(givenAnswer));
        AnswerEvaluation outcome = eval.matches(givenAnswer, acceptableAnswers);

        assertEquals(AnswerEvaluation.CORRECT_GUESS, outcome);
    }

    @Test
    public void expectCorrectGuessMultiple() {
        IAnswerEvaluator eval = new LevAnswerEvaluator();
        String givenAnswer = "soeren";
        Set<String> acceptableAnswers = new HashSet<>(Arrays.asList(givenAnswer, "wrong", "really-wrong"));
        AnswerEvaluation outcome = eval.matches(givenAnswer, acceptableAnswers);

        assertEquals(AnswerEvaluation.CORRECT_GUESS, outcome);
    }

    @Test
    public void expectAcceptableGuess() {
        IAnswerEvaluator eval = new LevAnswerEvaluator();
        String givenAnswer = "soeren";
        Set<String> acceptableAnswers = new HashSet<>(Arrays.asList("soerne"));
        AnswerEvaluation outcome = eval.matches(givenAnswer, acceptableAnswers);

        assertEquals(AnswerEvaluation.ACCEPTABLE_GUESS, outcome);
    }

    @Test
    public void expectIncorrectGuess() {
        IAnswerEvaluator eval = new LevAnswerEvaluator();
        String givenAnswer = "soeren";
        Set<String> acceptableAnswers = new HashSet<>(Arrays.asList("nereos"));
        AnswerEvaluation outcome = eval.matches(givenAnswer, acceptableAnswers);

        assertEquals(AnswerEvaluation.UNACCEPTABLE_GUESS, outcome);
    }
}
