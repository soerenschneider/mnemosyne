package mnemosyne.evaluator;

import mnemosyne.domain.Card;
import org.junit.Test;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;

public class EvaluationApplierTest {

    public static final double DELTA = 0.00001;

    @Test
    public void testSuccessfulGuess()
    {
        Card card = new Card();
        AnswerEvaluation eval = AnswerEvaluation.CORRECT_GUESS;

        assertEquals(0, card.getTimesAnswered());
        assertEquals(0, card.getTimesAnsweredCorrectly());
        assertNull(card.getDateAnsweredLastTime());
        assertEquals(0f, card.getSuccessfulAnswersRatio(), DELTA);

        IAnswerEvaluator evaluator = mock(IAnswerEvaluator.class);
        doCallRealMethod().when(evaluator).applyEvaluationResults(any(), any());

        evaluator.applyEvaluationResults(card, eval);

        assertEquals(1, card.getTimesAnswered());
        assertEquals(1, card.getTimesAnsweredCorrectly());
        assertNotNull(card.getDateAnsweredLastTime());
        assertEquals(100f, card.getSuccessfulAnswersRatio(), DELTA);
    }

    @Test
    public void testAcceptableGuess()
    {
        Card card = new Card();
        AnswerEvaluation eval = AnswerEvaluation.ACCEPTABLE_GUESS;

        assertEquals(0, card.getTimesAnswered());
        assertEquals(0, card.getTimesAnsweredCorrectly());
        assertNull(card.getDateAnsweredLastTime());
        assertEquals(0f, card.getSuccessfulAnswersRatio(), DELTA);

        IAnswerEvaluator evaluator = mock(IAnswerEvaluator.class);
        doCallRealMethod().when(evaluator).applyEvaluationResults(any(), any());

        evaluator.applyEvaluationResults(card, eval);

        assertEquals(1, card.getTimesAnswered());
        assertEquals(1, card.getTimesAnsweredCorrectly());
        assertNotNull(card.getDateAnsweredLastTime());
        assertEquals(100f, card.getSuccessfulAnswersRatio(), DELTA);
    }

    @Test
    public void testUnsuccessfulGuess()
    {
        Card card = new Card();
        AnswerEvaluation eval = AnswerEvaluation.UNACCEPTABLE_GUESS;

        assertEquals(0, card.getTimesAnswered());
        assertEquals(0, card.getTimesAnsweredCorrectly());
        assertNull(card.getDateAnsweredLastTime());
        assertEquals(0f, card.getSuccessfulAnswersRatio(), DELTA);

        IAnswerEvaluator evaluator = mock(IAnswerEvaluator.class);
        doCallRealMethod().when(evaluator).applyEvaluationResults(any(), any());

        evaluator.applyEvaluationResults(card, eval);

        assertEquals(1, card.getTimesAnswered());
        assertEquals(0, card.getTimesAnsweredCorrectly());
        assertNotNull(card.getDateAnsweredLastTime());
        assertEquals(0f, card.getSuccessfulAnswersRatio(), DELTA);
    }

    @Test
    public void testAll()
    {
        Card card = new Card();

        assertEquals(0, card.getTimesAnswered());
        assertEquals(0, card.getTimesAnsweredCorrectly());
        assertNull(card.getDateAnsweredLastTime());
        assertEquals(0f, card.getSuccessfulAnswersRatio(), DELTA);

        IAnswerEvaluator evaluator = mock(IAnswerEvaluator.class);
        doCallRealMethod().when(evaluator).applyEvaluationResults(any(), any());

        evaluator.applyEvaluationResults(card, AnswerEvaluation.UNACCEPTABLE_GUESS);
        evaluator.applyEvaluationResults(card, AnswerEvaluation.CORRECT_GUESS);
        evaluator.applyEvaluationResults(card, AnswerEvaluation.ACCEPTABLE_GUESS);

        assertEquals(3, card.getTimesAnswered());
        assertEquals(2, card.getTimesAnsweredCorrectly());
        assertNotNull(card.getDateAnsweredLastTime());
        assertEquals(66.66f, card.getSuccessfulAnswersRatio(), 0.01);
    }
}
