package mnemosyne.io;

import mnemosyne.io.importer.callbacks.ICsvImportCallback;
import mnemosyne.io.model.MnemosyneCsvRecord;

import java.util.ArrayList;
import java.util.List;

public class DummyCallback implements ICsvImportCallback {
    List<MnemosyneCsvRecord> records = new ArrayList<>();
    int flushCount = 0;

    @Override
    public void flush(List<MnemosyneCsvRecord> records) {
        this.records.addAll(records);
        this.flushCount += 1;
    }
}
