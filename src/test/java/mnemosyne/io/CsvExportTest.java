package mnemosyne.io;

import mnemosyne.domain.Card;
import mnemosyne.io.exporter.FastCsvExporter;
import mnemosyne.io.model.FlatCategory;
import mnemosyne.io.model.MnemosyneCsvRecord;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static junit.framework.TestCase.assertTrue;

public class CsvExportTest {
    @Test
    public void testCsvExport() throws Exception {
        ReadSourceDummyCallback callback = new ReadSourceDummyCallback(buildTestRecords(), 5);
        FastCsvExporter csv = new FastCsvExporter(callback);
        Path writtenCsv = Files.createTempFile("export.csv", ".tmp");
        csv.write(writtenCsv);

        Path expectedOutput = Paths.get("src/test/resources/expected-export.csv");
        assertTrue(FileUtils.contentEquals(expectedOutput.toFile(), writtenCsv.toFile()));
    }

    /**
     * Builds the objects with appropriate values that should produce the exact same output as the file
     * 'expected-export.csv"'.
     *
     * @return
     */
    private List<MnemosyneCsvRecord> buildTestRecords() {
        List<MnemosyneCsvRecord> records = new ArrayList<>();
        for (int index = 0; index <= 6; index++) {

            FlatCategory cat = new FlatCategory();
            cat.parentName = "Parent"+index;
            cat.categoryName = "Name"+index;
            cat.owner = "User"+index;

            Card card = new Card();
            card.setPrompt("Prompt"+index);
            Set<String> answers = new HashSet<>();
            card.setAnswers(answers);

            for (int inner = 1; inner <= index+1; inner++) {
                answers.add("answer"+index+""+inner);
            }

            card.setExample("Example Example Example"+index);

            LocalDateTime created = LocalDateTime.of(2018, 6,22,16,52, index);
            card.setDateCreated(created);

            LocalDateTime lastAnswered = LocalDateTime.of(2018, 6,22,16,55, index);
            card.setDateAnsweredLastTime(lastAnswered);

            card.setTimesAnswered(500 + index);
            card.setTimesAnsweredCorrectly(300 + index);

            MnemosyneCsvRecord record = new MnemosyneCsvRecord();
            record.category = cat;
            record.card = card;
            records.add(record);
        }

        return records;
    }
}
