package mnemosyne.io;

import mnemosyne.io.exporter.callbacks.IReadSourceCallback;
import mnemosyne.io.model.MnemosyneCsvRecord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReadSourceDummyCallback implements IReadSourceCallback {
    List<MnemosyneCsvRecord> records = new ArrayList<>();
    int flushSize;

    public ReadSourceDummyCallback(List<MnemosyneCsvRecord> records, int flushSize) {
        this.records = records;
        this.flushSize = flushSize;
    }

    @Override
    public List<MnemosyneCsvRecord> readPage(int page) {
        int from = page * flushSize;
        int to = Math.min((page + 1) * flushSize, records.size());

        if (from >= records.size() || to > records.size()) {
            return Collections.emptyList();
        }

        return records.subList(from, to);
    }
}
