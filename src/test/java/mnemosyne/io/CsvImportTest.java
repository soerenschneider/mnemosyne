package mnemosyne.io;

import mnemosyne.io.importer.FastCsvImporter;
import mnemosyne.io.importer.callbacks.ICsvImportCallback;
import mnemosyne.io.model.MnemosyneCsvRecord;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CsvImportTest {

    @Test
    public void readCsv() {
        int flushAfterRecords = 5;
        ICsvImportCallback callback = new DummyCallback();
        FastCsvImporter importer = new FastCsvImporter(callback, flushAfterRecords);

        Path file = Paths.get("src/test/resources/test.csv");
        try {
            importer.importFile(file);
        } catch (IOException e) {
            fail("Enocuntered exception: " + e.getMessage());
        }

        assertEquals(7, ((DummyCallback) callback).records.size());
    }

    @Test
    /**
     * Test that the parser respects the flushAfterRecords constraint. The csv contains 7 records with a flush
     * size set to 5. That makes 2 pages, therefore 2 flushes.
     */
    public void testFlushAmount() {
        int flushAfterRecords = 5;
        ICsvImportCallback callback = new DummyCallback();
        FastCsvImporter importer = new FastCsvImporter(callback, flushAfterRecords);

        Path file = Paths.get("src/test/resources/test.csv");
        try {
            importer.importFile(file);
        } catch (IOException e) {
            fail("Enocuntered exception: " + e.getMessage());
        }

        assertEquals(2, ((DummyCallback) callback).flushCount);
    }

    @Test
    public void readCsvContent() {
        int flushAfterRecords = 5;
        ICsvImportCallback callback = new DummyCallback();
        FastCsvImporter importer = new FastCsvImporter(callback, flushAfterRecords);

        Path file = Paths.get("src/test/resources/test.csv");
        try {
            importer.importFile(file);
        } catch (IOException e) {
            fail("Enocuntered exception: " + e.getMessage());
        }

        int index = 0;
        for (MnemosyneCsvRecord record : ((DummyCallback) callback).records) {
            assertEquals("Parent"+index, record.category.parentName);
            assertEquals("Name"+index, record.category.categoryName);
            assertEquals("User"+index, record.category.owner);

            assertEquals("Prompt"+index, record.card.getPrompt());
            assertEquals(index+1, record.card.getAnswers().size());

            int inner = 1;
            for (String answer : record.card.getAnswers()) {
                assertTrue(record.card.getAnswers().contains("answer"+index+""+inner));
                inner++;
            }

            assertEquals("Example Example Example"+index, record.card.getExample());

            LocalDateTime created = LocalDateTime.of(2018, 6,22,16,52, index);
            assertEquals(created, record.card.getDateCreated());
            LocalDateTime lastAnswered = LocalDateTime.of(2018, 6,22,16,55, index);
            assertEquals(lastAnswered, record.card.getDateAnsweredLastTime());

            assertEquals(500 + index, record.card.getTimesAnswered());
            assertEquals(300 + index, record.card.getTimesAnsweredCorrectly());

            index++;
        }
    }
}
