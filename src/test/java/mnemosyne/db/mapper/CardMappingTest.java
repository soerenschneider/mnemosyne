//package mnemosyne.db.mapper;
//
//import mnemosyne.db.entities.CardEntity;
//import mnemosyne.domain.Card;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.time.LocalDateTime;
//import java.util.Arrays;
//import java.util.HashSet;
//import java.util.Set;
//
//import static org.junit.Assert.assertEquals;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration
//public class CardMappingTest {
//    @Autowired
//    EntityMapper mapper;
//
//    @Test
//    public void testObjectMapping()
//    {
//        String owner = "soeren";
//        String prompt = "A";
//        Set<String> answers = new HashSet<>(Arrays.asList(new String[] {"B", "C"}));
//        String example = "An example";
//        int timesAnswered = 4;
//        int timesAnsweredCorrectly = 3;
//        LocalDateTime created = LocalDateTime.now();
//        LocalDateTime lastAnswered = LocalDateTime.now();
//        Set<String> tags = new HashSet<>(Arrays.asList(new String[] {"tag1", "tag2"}));
//
//        Card card = new Card();
//        card.setOwner(owner);
//        card.setPrompt(prompt);
//        card.setAnswers(answers);
//        card.setExample(example);
//        card.setTimesAnswered(timesAnswered);
//        card.setTimesAnsweredCorrectly(timesAnsweredCorrectly);
//        card.setDateAnsweredLastTime(lastAnswered);
//        card.setDateCreated(created);
//        card.setTags(tags);
//
//        CardEntity entity = mapper.map(card, CardEntity.class);
//
//        assertEquals(75., entity.getSuccessfulAnswersRatio(), 0.001);
//
//        Card converted = mapper.map(entity, Card.class);
//
//        assertEquals(owner, converted.getOwner());
//        assertEquals(prompt, converted.getPrompt());
//        assertEquals(answers, converted.getAnswers());
//        assertEquals(example, converted.getExample());
//        assertEquals(timesAnswered, converted.getTimesAnswered());
//        assertEquals(timesAnsweredCorrectly, converted.getTimesAnsweredCorrectly());
//        assertEquals(created, converted.getDateCreated());
//        assertEquals(lastAnswered, converted.getDateAnsweredLastTime());
//        assertEquals(tags, converted.getTags());
//        assertEquals(75., card.getSuccessfulAnswersRatio(), 0.001);
//    }
//
//    @Configuration
//    @ComponentScan("mnemosyne.db.mapper")
//    static class SpringTestConfig {
//
//    }
//}
