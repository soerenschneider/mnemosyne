package mnemosyne.game.hints;

import mnemosyne.domain.Card;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SimpleHintGeneratorTest {
    @Test
    public void expectSingleSubstitution() {
        IHintGenerator generator = new SimpleHintGenerator();
        Card card = new Card();
        card.setAnswers(new HashSet<>(Arrays.asList("test")));

        String hint = generator.generateHint(card);
        assertEquals(1, StringUtils.countMatches(hint, '*'));
    }

    @Test
    public void expectTwoSubstitution() {
        IHintGenerator generator = new SimpleHintGenerator();
        Card card = new Card();
        card.setAnswers(new HashSet<>(Arrays.asList("yesyes")));

        String hint = generator.generateHint(card);
        assertEquals(2, StringUtils.countMatches(hint, '*'));
    }

    @Test
    public void dontSubstituteWhitespaces() {
        IHintGenerator generator = new SimpleHintGenerator();
        Card card = new Card();
        card.setAnswers(new HashSet<>(Arrays.asList("a  bc")));

        String hint = generator.generateHint(card);
        assertEquals(1, StringUtils.countMatches(hint, '*'));
        assertTrue(hint.contains(" "));
    }

    @Test
    public void expectNoSubstitutions() {
        IHintGenerator generator = new SimpleHintGenerator();
        Card card = new Card();
        String input = "no";
        card.setAnswers(new HashSet<>(Arrays.asList(input)));

        String hint = generator.generateHint(card);
        assertEquals(input, hint);
    }
}
