package mnemosyne.security;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

/**
 * Creates and verifies JW tokens.
 */
@Component
@Slf4j
public class JwtTokenProvider {
    @Value("${spring.security.jwt.secret}")
    private String jwtSecret;

    @Value("${spring.security.jwt.expirationTimeInSecs}")
    private int jwtExpirationInSecs;

    /**
     * Generates a token for a given {@link Authentication} object.
     *
     * @param authentication spring auth container.
     * @return JWT as a base64 encoded string.
     */
    public String generateToken(Authentication authentication) {
        UserPrincipal userDetails = (UserPrincipal) authentication.getPrincipal();

        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime expiryDate = now.plusSeconds(jwtExpirationInSecs);

        return Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setIssuedAt(Date.from(now.toInstant()))
                .setExpiration(Date.from(expiryDate.toInstant()))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    /**
     * Extracts the subject from the JWT claims.
     *
     * @param token the JWT token.
     * @return the subject.
     */
    public Optional<String> getUserIdFromJWT(String token) {
        if (StringUtils.isBlank(token)) {
            return Optional.empty();
        }

        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return Optional.of(claims.getSubject());
    }

    /**
     * Validates a JWT.
     *
     * @param authToken the token to validate.
     * @return true if (still) valid, otherwise false.
     */
    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            log.debug("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            log.debug("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.debug("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.debug("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.debug("JWT claims string is empty.");
        }
        return false;
    }
}
