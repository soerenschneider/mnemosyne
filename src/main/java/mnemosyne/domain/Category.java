package mnemosyne.domain;

import javax.validation.constraints.NotEmpty;
import java.util.Objects;

public class Category {
    private int id;

    private Category parent = null;

    private String name;

    @NotEmpty(message = "field must not be empty")
    private String owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return id == category.id &&
                Objects.equals(parent, category.parent) &&
                Objects.equals(name, category.name) &&
                Objects.equals(owner, category.owner);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, parent, name, owner);
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", parent=" + parent +
                ", name='" + name + '\'' +
                ", owner='" + owner + '\'' +
                '}';
    }
}
