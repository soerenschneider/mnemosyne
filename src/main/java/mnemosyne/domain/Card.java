package mnemosyne.domain;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Holds the logic for a single prompt.
 */
public class Card {
    /**
     * Primary key.
     */
    private long id;

    /**
     * The prompt of this card the user will have to answer.
     */
    private String prompt;

    /**
     * Accepted answers for the prompt.
     */
    private Set<String> answers;

    /**
     * An example of the prompt.
     */
    private String example;

    /**
     * The amount of times this card has been answered.
     */
    private int timesAnswered;

    /**
     * The amount of times this card has been answered correctly.
     */
    private int timesAnsweredCorrectly;

    private Category category;

    /**
     * The date when this card has been answered for the last time.
     */
    private LocalDateTime dateAnsweredLastTime;

    /**
     * The date when this card has been created.
     */
    private LocalDateTime dateCreated;

    public Card() {
        // Automatically set created date
        this.dateCreated = LocalDateTime.now();
    }

    /**
     * Determines the ratio of successful guesses to total guesses.
     *
     * @return
     */
    public float getSuccessfulAnswersRatio() {
        if (0 == timesAnswered) {
            return 0f;
        }

        return timesAnsweredCorrectly * 100f / timesAnswered;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return timesAnswered == card.timesAnswered &&
                timesAnsweredCorrectly == card.timesAnsweredCorrectly &&
                Objects.equals(id, card.id) &&
                Objects.equals(prompt, card.prompt) &&
                Objects.equals(answers, card.answers) &&
                Objects.equals(example, card.example) &&
                Objects.equals(category, card.category) &&
                Objects.equals(dateAnsweredLastTime, card.dateAnsweredLastTime) &&
                Objects.equals(dateCreated, card.dateCreated);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, prompt, answers, example, timesAnswered, timesAnsweredCorrectly, category, dateAnsweredLastTime, dateCreated);
    }


    // -----------------

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public void addAnswer(String answer) {
        if (null == this.answers) {
            this.answers = new HashSet<>();
        }
        this.answers.add(answer);
    }

    public Set<String> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<String> answers) {
        this.answers = answers;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public int getTimesAnswered() {
        return timesAnswered;
    }

    public void setTimesAnswered(int timesAnswered) {
        this.timesAnswered = timesAnswered;
    }

    public int getTimesAnsweredCorrectly() {
        return timesAnsweredCorrectly;
    }

    public void setTimesAnsweredCorrectly(int timesAnsweredCorrectly) {
        this.timesAnsweredCorrectly = timesAnsweredCorrectly;
    }

    public LocalDateTime getDateAnsweredLastTime() {
        return dateAnsweredLastTime;
    }

    public void setDateAnsweredLastTime(LocalDateTime dateAnsweredLastTime) {
        this.dateAnsweredLastTime = dateAnsweredLastTime;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", prompt='" + prompt + '\'' +
                ", answers=" + answers +
                ", example='" + example + '\'' +
                ", timesAnswered=" + timesAnswered +
                ", timesAnsweredCorrectly=" + timesAnsweredCorrectly +
                ", category=" + category +
                ", dateAnsweredLastTime=" + dateAnsweredLastTime +
                ", dateCreated=" + dateCreated +
                '}';
    }
}