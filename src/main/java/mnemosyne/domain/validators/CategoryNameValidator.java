package mnemosyne.domain.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validates a category name.
 */
public class CategoryNameValidator implements ConstraintValidator<CategoryNameConstraint, String> {

    @Override
    public void initialize(CategoryNameConstraint name) {
    }

    @Override
    public boolean isValid(String name, ConstraintValidatorContext cxt) {
        return name != null && name.length() > 3 && !name.contains("/");
    }
}
