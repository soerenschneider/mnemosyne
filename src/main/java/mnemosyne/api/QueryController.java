package mnemosyne.api;

import lombok.extern.slf4j.Slf4j;
import mnemosyne.api.payload.CurrentCard;
import mnemosyne.api.payload.GameCard;
import mnemosyne.cache.Session;
import mnemosyne.cache.SessionRepo;
import mnemosyne.db.dao.cards.CardsRepo;
import mnemosyne.domain.Card;
import mnemosyne.security.CurrentUser;
import mnemosyne.security.UserPrincipal;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Controller for user based queries.
 */
@Slf4j
@RestController
public class QueryController {

    @Autowired
    private CardsRepo cardsRepo;

    @Autowired
    private SessionRepo repo;

    @RequestMapping(value="/categories/{id}/cards", method=RequestMethod.GET)
    public List<Card> read(@PathVariable("id") Integer id)
    {
        return cardsRepo.findByCategory(id);
    }

    @RequestMapping(value="/cards/current", method=RequestMethod.GET)
    public CurrentCard read(@CurrentUser UserPrincipal user)
    {
        Optional<Session> session = repo.findById(user.getUsername());
        if (! session.isPresent()) {
            return CurrentCard.builder().message("No session").ok(false).build();
        }

        Long currentId = session.get().getCurrentCardId();
        if (null != currentId) {
            long id = currentId;
            Card card = cardsRepo.findCard(id).orElse(null);
            return CurrentCard.builder().card(new GameCard(card)).ok(true).build();
        }

        return CurrentCard.builder().build();
    }
}
