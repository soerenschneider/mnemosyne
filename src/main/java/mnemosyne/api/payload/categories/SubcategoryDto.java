package mnemosyne.api.payload.categories;

import mnemosyne.db.pojos.FlatCategories;

public class SubcategoryDto {
    public String name;
    public int id;
    public long count;

    public SubcategoryDto(FlatCategories flat) {
        this.name = flat.category_name;
        this.id = flat.category_id;
        this.count = flat.count;
    }

    public SubcategoryDto(String name, int id, long count) {
        this.name = name;
        this.id = id;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public long getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "SubcategoryDto{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", count=" + count +
                '}';
    }
}
