package mnemosyne.api.payload.categories;

import mnemosyne.db.pojos.FlatCategories;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HierachCategories {
    public List<CategoryDto> categories = new ArrayList<>();

    public HierachCategories(List<FlatCategories> list) {
        // Tmp data
        Map<Integer, CategoryDto> parents = new HashMap<>();
        Map<Integer, List<SubcategoryDto>> subcategories = new HashMap<>();

        for (FlatCategories cat : list) {
            if (cat.isParentCategory) {
                CategoryDto category = new CategoryDto(cat);
                if (! parents.containsKey(category.id)) {
                    parents.put(category.id, category);
                }
            } else {
                SubcategoryDto sub = new SubcategoryDto(cat);
                if (! subcategories.containsKey(cat.parent_id)) {
                    subcategories.put(cat.parent_id, new ArrayList<>());
                }
                subcategories.get(cat.parent_id).add(sub);
            }
        }

        for (Integer parent_id : parents.keySet()) {
            CategoryDto parent = parents.get(parent_id);
            List<SubcategoryDto> subs = subcategories.get(parent_id);

            parent.subcategories = subs;
            this.categories.add(parent);
        }

        parents.clear();
        subcategories.clear();
    }
}
