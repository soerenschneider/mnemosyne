package mnemosyne.api.payload.categories;

import mnemosyne.db.pojos.FlatCategories;

import java.util.ArrayList;
import java.util.List;

public class CategoryDto {
    public String name;
    public int id;
    public long count;

    public List<SubcategoryDto> subcategories;

    public CategoryDto() {
        this.subcategories = new ArrayList<>();
    }

    public CategoryDto(FlatCategories flat) {
        this.name = flat.category_name;
        this.id = flat.category_id;
        this.count = flat.count;
        this.count = flat.count;

        this.subcategories = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public long getCount() {
        return count;
    }

    public List<SubcategoryDto> getSubcategories() {
        return subcategories;
    }

    @Override
    public String toString() {
        return "CategoryDto{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", count=" + count +
                ", subcategories=" + subcategories +
                '}';
    }
}
