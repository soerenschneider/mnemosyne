package mnemosyne.api.payload;

import mnemosyne.domain.Card;
import mnemosyne.domain.Category;

public class GameCard {
    public final long id;
    public final String prompt;
    public final String example;
    public final float ratio;
    public final Category category;

    public GameCard(Card card) {
        this.id = card.getId();
        this.category = card.getCategory();
        this.prompt = card.getPrompt();
        this.example = card.getExample();
        this.ratio = card.getSuccessfulAnswersRatio();
    }
}
