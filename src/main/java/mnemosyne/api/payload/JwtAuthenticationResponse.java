package mnemosyne.api.payload;

import lombok.Builder;

@Builder
public class JwtAuthenticationResponse {
    public final String accessToken;
    public final String tokenType = "Bearer";
}
