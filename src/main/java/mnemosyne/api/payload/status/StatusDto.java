package mnemosyne.api.payload.status;

public class StatusDto {
    public final CardDto card = new CardDto();
    public final SessionDto session = new SessionDto();
}
