package mnemosyne.api.payload.status;

public class SessionDto {
    public int cards_left;

    public int guesses;

    public int correct_guesses;

    public int total_guesses;

    public int total_correct_guesses;

    public long current_card_id;
}
