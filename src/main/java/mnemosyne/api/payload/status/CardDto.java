package mnemosyne.api.payload.status;

public class CardDto {
    public long id;
    public String prompt;
    public String example;
}
