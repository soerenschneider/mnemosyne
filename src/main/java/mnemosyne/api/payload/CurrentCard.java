package mnemosyne.api.payload;

import lombok.Builder;
import lombok.ToString;
import mnemosyne.domain.Card;

@Builder
@ToString
public class CurrentCard {
    public final GameCard card;
    public final Boolean ok;
    public final String message;
}
