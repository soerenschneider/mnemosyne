package mnemosyne.api.payload;

import javax.validation.constraints.NotEmpty;

public class TagsQuery {
    @NotEmpty
    public String[] tags;
}
