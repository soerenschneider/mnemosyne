package mnemosyne.api;

import lombok.extern.slf4j.Slf4j;
import mnemosyne.api.payload.categories.HierachCategories;
import mnemosyne.db.dao.categories.CategoriesRepo;
import mnemosyne.domain.Category;
import mnemosyne.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * Controller for user based queries.
 */
@Slf4j
@RestController
public class CategoriesQueryController {

    @Autowired
    private CategoriesRepo repo;

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public HierachCategories readCategories()
    {
        return repo.getHierarchicalCategories(getUsername());
    }

    @RequestMapping(value = "/categories/{id}", method = RequestMethod.GET)
    public Optional<Category> readCategory(@PathVariable("id") Integer id)
    {
        return repo.find(id, getUsername());
    }

    private String getUsername() {
        UserPrincipal p = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return p.getUsername();
    }
}
