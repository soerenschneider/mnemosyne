package mnemosyne.api;

import lombok.extern.slf4j.Slf4j;
import mnemosyne.commands.CommandBus;
import mnemosyne.commands.categories.createcategory.CreateCategoryCmd;
import mnemosyne.commands.categories.createcategory.CreateCategoryOutcome;
import mnemosyne.commands.categories.deletecategory.DeleteCategoryCmd;
import mnemosyne.commands.categories.deletecategory.DeleteCategoryOutcome;
import mnemosyne.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

/**
 * Controller accepting commands. 
 */
@RestController
@Slf4j
public class CategoriesController {

    @Autowired
    private CommandBus ingester;

    private String getUsername() {
        UserPrincipal p = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return p.getUsername();
    }

    @RequestMapping(value = "/categories", method = RequestMethod.POST)
    public ResponseEntity<CreateCategoryOutcome> createCategory(@RequestBody @Valid CreateCategoryCmd cmd)
    {
        CreateCategoryOutcome outcome = (CreateCategoryOutcome) ingester.acceptCommand(cmd);
        return ResponseEntity.ok(outcome);
    }

    @RequestMapping(value = "/categories/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<DeleteCategoryOutcome> deleteCategory(@PathParam("id") String id)
    {
        DeleteCategoryOutcome outcome = (DeleteCategoryOutcome) ingester.acceptCommand(new DeleteCategoryCmd(id));
        return ResponseEntity.ok(outcome);
    }
}
