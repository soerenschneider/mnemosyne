package mnemosyne.api;

import lombok.extern.slf4j.Slf4j;
import mnemosyne.api.payload.CurrentCard;
import mnemosyne.api.payload.GameCard;
import mnemosyne.api.payload.status.StatusDto;
import mnemosyne.cache.CardsSequence;
import mnemosyne.cache.Session;
import mnemosyne.cache.SessionRepo;
import mnemosyne.db.dao.cards.CardsRepo;
import mnemosyne.domain.Card;
import mnemosyne.security.CurrentUser;
import mnemosyne.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * Controller for user based queries.
 */
@Slf4j
@RestController
public class StatusQueryController {

    @Autowired
    private CardsRepo cardsRepo;

    @Autowired
    private SessionRepo repo;

    @Autowired
    private CardsSequence sequenceRepo;

    @RequestMapping(value="/status", method=RequestMethod.GET)
    public Optional<StatusDto> read(@CurrentUser UserPrincipal user)
    {
        final String username = user.getUsername();

        Optional<Session> session = repo.findById(username);
        if (! session.isPresent()) {
            return Optional.empty();
        }

        StatusDto s = new StatusDto();
        s.session.cards_left = (int) sequenceRepo.getSize(username);

        s.session.guesses = session.get().getGuesses();
        s.session.correct_guesses = session.get().getCorrectGuesses();

        s.session.total_correct_guesses = session.get().getTotalCorrectGuesses();
        s.session.total_guesses = session.get().getTotalGuesses();

        Long currentId = session.get().getCurrentCardId();
        if (null != currentId) {
            long id = currentId;
            Optional<Card> card = cardsRepo.findCard(id);
            if (card.isPresent()) {
                s.card.id = id;
                s.card.prompt = card.get().getPrompt();
                s.session.current_card_id = id;
                s.card.example = card.get().getExample();
            }
        }

        return Optional.of(s);
    }
}
