package mnemosyne.api;

import lombok.extern.slf4j.Slf4j;
import mnemosyne.io.CsvImportExport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Controller for user based queries.
 */
@Slf4j
@RestController
public class ImportExportController {
    @Autowired
    private CsvImportExport csv;

    @RequestMapping(value = "/import/csv", method = RequestMethod.POST)
    public void uploadFileHandler(@RequestParam("file") MultipartFile file) {

        // TODO: Move code away from REST controller

        if (null == file || file.isEmpty()) {
            // TODO: Message client
            return;
        }

        Path path = null;
        try {
            path = Files.createTempFile(file.getOriginalFilename(), ".tmp");
            File convertedFile = path.toFile();
            file.transferTo(convertedFile);
            csv.importCsv(path);
        } catch (IOException e) {
            log.error("Error processing csv file upload: {}", e.getMessage());
        } finally {
            if (null != path) {
                path.toFile().delete();
            }
        }
    }

    private String filename() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        return LocalDateTime.now().format(formatter);
    }

    @RequestMapping(value = "/export/csv", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> export() {
        Path path = null;
        try {
            String filename = "mnemosyne-csv-"+filename()+".csv";
            path = Files.createTempFile("export", ".tmp");
            csv.exportCsv(path);
            File file = path.toFile();
            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

            return ResponseEntity.ok().headers(headers)
                    .contentLength(file.length())
                    .contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);


        } catch (IOException e) {
            log.error("Error processing csv download request: {}", e.getMessage());
        }

        return null;
    }
}
