package mnemosyne.api;

import lombok.extern.slf4j.Slf4j;
import mnemosyne.commands.CommandBus;
import mnemosyne.commands.CommandOutcome;
import mnemosyne.commands.GenericCommandOutcome;
import mnemosyne.commands.cards.createcard.CreateCardCmd;
import mnemosyne.commands.cards.createcard.CreateCardOutcome;
import mnemosyne.commands.cards.deletecard.DeleteCardCmd;
import mnemosyne.commands.cards.deletecard.DeleteCardOutcome;
import mnemosyne.commands.cards.guess.GuessCmd;
import mnemosyne.commands.cards.guess.GuessCmdOutcome;
import mnemosyne.commands.cards.loadtags.LoadTagsCmd;
import mnemosyne.commands.cards.requesthint.RequestHintCmd;
import mnemosyne.commands.cards.requesthint.RequestHintOutcome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Controller accepting commands. 
 */
@RestController
@Slf4j
public class CommandController {
    @Autowired
    private CommandBus ingester;

    @RequestMapping(value = "/guess", method = RequestMethod.POST)
    public ResponseEntity<GuessCmdOutcome> guess(@Valid @RequestBody GuessCmd cmd)
    {
        GuessCmdOutcome outcome = (GuessCmdOutcome) ingester.acceptCommand(cmd);
        return ResponseEntity.ok(outcome);
    }

    @RequestMapping(value = "/hint", method = RequestMethod.GET)
    public ResponseEntity<RequestHintOutcome> requestHint()
    {
        RequestHintOutcome outcome = (RequestHintOutcome) ingester.acceptCommand(new RequestHintCmd());
        return ResponseEntity.ok(outcome);
    }

    @RequestMapping(value = "/cards", method = RequestMethod.POST)
    public ResponseEntity<CreateCardOutcome> createCard(@Valid @RequestBody CreateCardCmd cmd)
    {
        CreateCardOutcome outcome = (CreateCardOutcome) ingester.acceptCommand(cmd);
        if (outcome.ok)
        {
            return ResponseEntity.ok(outcome);
        }

        return ResponseEntity.unprocessableEntity().body(outcome);
    }

    @RequestMapping(value = "/cards", method = RequestMethod.DELETE)
    public ResponseEntity<DeleteCardOutcome> deleteCard(@Valid @RequestBody DeleteCardCmd cmd)
    {
        DeleteCardOutcome outcome = (DeleteCardOutcome) ingester.acceptCommand(cmd);
        if (outcome.ok)
        {
            return ResponseEntity.ok().body(outcome);
        }

        return ResponseEntity.unprocessableEntity().body(outcome);
    }

    @RequestMapping(value = "/loadtags", method = RequestMethod.POST)
    public ResponseEntity<CommandOutcome> loadTags(@Valid @RequestBody LoadTagsCmd cmd)
    {
        GenericCommandOutcome outcome = (GenericCommandOutcome) ingester.acceptCommand(cmd);
        if (outcome.ok)
        {
            return ResponseEntity.ok().body(outcome);
        }

        return ResponseEntity.unprocessableEntity().body(outcome);
    }

//    @RequestMapping(value = "/import", method = RequestMethod.POST)
//    public ResponseEntity<CommandOutcome> handleFileUpload(@RequestParam("csv") @Validated  MultipartFile csv)
//    {
//        CsvReader reader = new CsvReader();
//
//        ImportCardsCmd cmd = new ImportCardsCmd();
//        cmd.cards = reader.read(csv);
//
//        ImportCardsOutcome outcome = (ImportCardsOutcome) ingester.acceptCommand(cmd);
//        if (outcome.ok)
//        {
//            return ResponseEntity.ok().body(outcome);
//        }
//
//        return ResponseEntity.unprocessableEntity().body(outcome);
//    }


}
