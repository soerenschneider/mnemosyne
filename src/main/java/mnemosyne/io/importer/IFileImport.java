package mnemosyne.io.importer;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Provides file based import mechanism.
 */
public interface IFileImport {

    /** Defines after how many processed records usually to flush. Can be overriden by client. */
    int FLUSH_AFTER_RECORDS = 300;

    void importFile(Path csvfile) throws IOException;
}
