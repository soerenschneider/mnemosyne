package mnemosyne.io.importer;

import de.siegmar.fastcsv.reader.CsvParser;
import de.siegmar.fastcsv.reader.CsvReader;
import de.siegmar.fastcsv.reader.CsvRow;
import lombok.NonNull;
import mnemosyne.io.CsvMapper;
import mnemosyne.io.importer.callbacks.ICsvImportCallback;
import mnemosyne.io.model.MnemosyneCsvRecord;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Performs an input of CSV data using the FastCSV library.
 */
public class FastCsvImporter implements IFileImport {

    private ICsvImportCallback callback;

    private CsvMapper mapper = new CsvMapper();

    private final int flushAfterRecords;

    public FastCsvImporter(@NonNull ICsvImportCallback callback) {
        this.callback = callback;

        // Take the default from the interface
        this.flushAfterRecords = FLUSH_AFTER_RECORDS;
    }

    public FastCsvImporter(@NonNull ICsvImportCallback callback, int flushAfterNRecords) {
        this.callback = callback;

        if (flushAfterNRecords < 5) {
            throw new IllegalArgumentException("'flushAfterNRecords' must not be < 5");
        }

        this.flushAfterRecords = flushAfterNRecords;
    }

    @Override
    public void importFile(Path file) throws IOException {
        CsvReader csvReader = new CsvReader();
        csvReader.setContainsHeader(true);
        csvReader.setFieldSeparator(';');

        int readRecords = 1;

        List<MnemosyneCsvRecord> entries = new ArrayList<>(flushAfterRecords);

        // Parse the data efficiently: use chunks
        try (CsvParser csvParser = csvReader.parse(file, StandardCharsets.UTF_8)) {
            CsvRow row;
            while ((row = csvParser.nextRow()) != null) {

                MnemosyneCsvRecord entry = mapper.from(row.getFieldMap());
                entries.add(entry);

                if (readRecords % flushAfterRecords == 0) {
                    callback.flush(entries);
                    entries.clear();
                }

                readRecords++;
            }
        }

        callback.flush(entries);
    }
}
