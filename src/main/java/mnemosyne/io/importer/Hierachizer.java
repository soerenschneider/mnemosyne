package mnemosyne.io.importer;

import mnemosyne.domain.Card;
import mnemosyne.io.model.FlatCategory;
import mnemosyne.io.model.MnemosyneCsvRecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hierachizer {
    public Map<FlatCategory, List<Card>> flush(List<MnemosyneCsvRecord> records) {
        Map<FlatCategory, List<Card>> entries = new HashMap<>();

        for (MnemosyneCsvRecord record : records) {
            if (!entries.containsKey(record.category)) {
                entries.put(record.category, new ArrayList<>());
            }
            entries.get(record.category).add(record.card);
        }

        return entries;
    }
}
