package mnemosyne.io.importer.callbacks;

import lombok.extern.slf4j.Slf4j;
import mnemosyne.db.dao.cards.CardsRepo;
import mnemosyne.db.dao.categories.CategoriesRepo;
import mnemosyne.domain.Card;
import mnemosyne.domain.Category;
import mnemosyne.io.importer.Hierachizer;
import mnemosyne.io.model.FlatCategory;
import mnemosyne.io.model.MnemosyneCsvRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Callback that writes read data to the database.
 */
@Component
@Slf4j
public class DbCsvImportCallback implements ICsvImportCallback {
    @Autowired
    private CategoriesRepo categoriesRepo;

    @Autowired
    private CardsRepo cardRepo;

    @Override
    public void flush(List<MnemosyneCsvRecord> bla) {
        Map<FlatCategory, List<Card>> records = new Hierachizer().flush(bla);

        log.debug("Writing {} csv records to the DB", records.size());

        List<Card> cards = new ArrayList<>();
        for (FlatCategory categoryKey : records.keySet()) {
            Category category = categoriesRepo.createOrFind(categoryKey.parentName, categoryKey.categoryName, categoryKey.owner);

            for (Card card : records.get(categoryKey)) {
                card.setCategory(category);
                cards.add(card);
            }
        }
        cardRepo.saveMultiple(cards);
    }
}
