package mnemosyne.io.importer.callbacks;

import mnemosyne.io.model.MnemosyneCsvRecord;

import java.util.List;

/**
 * Callback to de-couple the code of reading data from the processing of the
 * read data.
 */
public interface ICsvImportCallback {
    /**
     * Write a read batch.
     * @param records
     */
    void flush(List<MnemosyneCsvRecord> records);
}
