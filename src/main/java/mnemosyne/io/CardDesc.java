package mnemosyne.io;

/**
 * Describes the headers of CSV data.
 */
public enum CardDesc {
    PROMPT,
    ANSWERS,
    EXAMPLE,
    CATEGORY_NAME,
    CATEGORY_PARENT,
    CATEGORY_OWNER,
    TIMES_ANSWERED,
    TIMES_ANSWERED_CORRECTLY,
    DATE_ANSWERED_LAST_TIME,
    DATE_CREATED
}
