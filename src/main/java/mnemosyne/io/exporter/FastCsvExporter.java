package mnemosyne.io.exporter;

import de.siegmar.fastcsv.writer.CsvAppender;
import de.siegmar.fastcsv.writer.CsvWriter;
import lombok.NonNull;
import mnemosyne.io.CsvMapper;
import mnemosyne.io.exporter.callbacks.IReadSourceCallback;
import mnemosyne.io.model.MnemosyneCsvRecord;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;

/**
 * File export implementation using FastCsv library.
 */
public class FastCsvExporter implements IFileExport {
    private final CsvMapper mapper = new CsvMapper();

    private final IReadSourceCallback callback;

    public FastCsvExporter(@NonNull IReadSourceCallback callback) {
        this.callback = callback;
    }

    public void write(Path csvFile) throws IOException  {
        CsvWriter csvWriter = new CsvWriter();
        csvWriter.setFieldSeparator(';');

        try (CsvAppender csvAppender = csvWriter.append(csvFile, StandardCharsets.UTF_8)) {
            csvAppender.appendLine(mapper.getHeader());

            int page = 0;
            List<MnemosyneCsvRecord> records = null;
            while ((records = callback.readPage(page)) != null && !records.isEmpty()) {

                for (MnemosyneCsvRecord record : records) {
                    csvAppender.appendLine(mapper.toMap(record));
                }

                csvAppender.flush();
                page += 1;
            }

            csvAppender.flush();
        }
    }

}
