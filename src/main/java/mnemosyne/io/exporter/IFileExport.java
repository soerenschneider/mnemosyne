package mnemosyne.io.exporter;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Export the database to a file.
 */
public interface IFileExport {
    void write(Path csvFile) throws IOException;
}
