package mnemosyne.io.exporter.callbacks;

import mnemosyne.db.dao.cards.CardsRepo;
import mnemosyne.db.mapper.EntityMapper;
import mnemosyne.db.pojos.MnemosyneDbRecord;
import mnemosyne.domain.Card;
import mnemosyne.io.model.MnemosyneCsvRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Callback implementation provides the exporter with data from the persistence layer.
 */
@Component
public class ReadDbImpl implements IReadSourceCallback {

    @Autowired
    private CardsRepo repo;

    @Autowired
    private EntityMapper mapper;

    @Override
    public List<MnemosyneCsvRecord> readPage(int page) {
        List<MnemosyneDbRecord> records = repo.dump(page);
        return records.stream().map(x -> fromDbRecord(x)).collect(Collectors.toList());
    }

    /**
     * Maps the almost identical {@link MnemosyneDbRecord} to a {@link MnemosyneCsvRecord}.
     *
     * @param record the record to map.
     * @return the mapped record.
     */
    private final MnemosyneCsvRecord fromDbRecord(MnemosyneDbRecord record) {
        MnemosyneCsvRecord target = new MnemosyneCsvRecord();
        target.category = record.category;
        target.card = mapper.map(record.card, Card.class);
        return target;
    }
}
