package mnemosyne.io.exporter.callbacks;

import mnemosyne.io.model.MnemosyneCsvRecord;

import java.util.List;

/**
 * De-couples the code of writing file based data from reading from the persistence layer.
 */
public interface IReadSourceCallback {
    /**
     * Reads from the persistence layer until all appropriate records have been returned.
     *
     * @param page the appropriate page to read. The first value must be 0.
     * @return returns a list of database records until the end is reached. If there are no more
     * entries left to read, an empty list is returned.
     */
    List<MnemosyneCsvRecord> readPage(int page);
}
