package mnemosyne.io;

import mnemosyne.domain.Card;
import mnemosyne.io.model.MnemosyneCsvRecord;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Provides mapping between a card and Map<String, String>.
 */
public class CsvMapper {
    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public String[] toMap(MnemosyneCsvRecord record) {
        String[] entry = new String[10];

        entry[0] = record.category.parentName;
        entry[1] = record.category.categoryName;
        entry[2] = record.category.owner;

        entry[3] = record.card.getPrompt();
        entry[4] = String.join(",", record.card.getAnswers());
        entry[5] = record.card.getExample();
        LocalDateTime dateCreated = record.card.getDateCreated();
        if (null != dateCreated) {
            entry[6] = formatter.format(dateCreated);
        }

        LocalDateTime dateAnsweredLastTime = record.card.getDateAnsweredLastTime();
        if (null != dateAnsweredLastTime) {
            entry[7] = formatter.format(dateAnsweredLastTime);
        }

        entry[8] = String.valueOf(record.card.getTimesAnswered());
        entry[9] = String.valueOf(record.card.getTimesAnsweredCorrectly());

        return entry;
    }

    /**
     * Defines the fixed header each CSV file needs to fulfill to import data.
     * @return the list and order of the CSV columns.
     */
    public String[] getHeader() {
        String[] header = new String[10];
        header[0] = CardDesc.CATEGORY_PARENT.toString();
        header[1] = CardDesc.CATEGORY_NAME.toString();
        header[2] = CardDesc.CATEGORY_OWNER.toString();
        header[3] = CardDesc.PROMPT.toString();
        header[4] = CardDesc.ANSWERS.toString();
        header[5] = CardDesc.EXAMPLE.toString();
        header[6] = CardDesc.DATE_CREATED.toString();
        header[7] = CardDesc.DATE_ANSWERED_LAST_TIME.toString();
        header[8] = CardDesc.TIMES_ANSWERED.toString();
        header[9] = CardDesc.TIMES_ANSWERED_CORRECTLY.toString();

        return header;
    }

    /**
     * Implementation-independent mechanism to convert an entry read from CSV to a insertable record.
     *
     * @param record
     * @return
     */
    public MnemosyneCsvRecord from(Map<String, String> record) {
        String categoryParent = record.get(CardDesc.CATEGORY_PARENT.toString());
        String categoryName = record.get(CardDesc.CATEGORY_NAME.toString());
        String categoryOwner = record.get(CardDesc.CATEGORY_OWNER.toString());

        Card card = new Card();
        card.setPrompt(record.get(CardDesc.PROMPT.toString()));

        String answers = record.get(CardDesc.ANSWERS.toString());
        if (answers != null) {
            // Split and remove whitespaces
            String[] s = Arrays.stream(answers.split(",")).map(String::trim).toArray(String[]::new);
            Set<String> answersSet = new HashSet<>(Arrays.asList(s));
            card.setAnswers(answersSet);
        }

        card.setExample(record.get(CardDesc.EXAMPLE.toString()));

        String dateAnsweredLastTime = record.get(CardDesc.DATE_ANSWERED_LAST_TIME.toString());
        if (null != dateAnsweredLastTime && !dateAnsweredLastTime.isEmpty()) {
            card.setDateAnsweredLastTime(LocalDateTime.parse(dateAnsweredLastTime, formatter));
        }

        String dateCreated = record.get(CardDesc.DATE_CREATED.toString());
        if (null != dateCreated && !dateCreated.isEmpty()) {
            card.setDateCreated(LocalDateTime.parse(dateCreated, formatter));
        }

        String timesAnswered = record.get(CardDesc.TIMES_ANSWERED.toString());
        if (null != timesAnswered) {
            card.setTimesAnswered(Integer.valueOf(timesAnswered).intValue());
        }

        String timesAnsweredCorrectly = record.get(CardDesc.TIMES_ANSWERED_CORRECTLY.toString());
        if (null != timesAnsweredCorrectly) {
            card.setTimesAnsweredCorrectly(Integer.valueOf(timesAnsweredCorrectly).intValue());
        }

        return new MnemosyneCsvRecord(categoryParent, categoryName, categoryOwner, card);
    }
}