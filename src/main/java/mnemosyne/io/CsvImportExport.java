package mnemosyne.io;

import mnemosyne.io.exporter.FastCsvExporter;
import mnemosyne.io.exporter.IFileExport;
import mnemosyne.io.exporter.callbacks.ReadDbImpl;
import mnemosyne.io.importer.callbacks.DbCsvImportCallback;
import mnemosyne.io.importer.FastCsvImporter;
import mnemosyne.io.importer.IFileImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Performs backup operations with respect to importing and exporting data
 * to/from the system.
 */
@Component
public class CsvImportExport {

    @Autowired
    private DbCsvImportCallback callback;

    @Autowired
    private ReadDbImpl source;

    /**
     * Imports a given CSV file to the system.
     *
     * @param file the path to the file.
     * @throws IOException
     */
    public void importCsv(Path file) throws IOException {
        IFileImport importer = new FastCsvImporter(callback);
        importer.importFile(file);
    }

    /**
     * Exports the data in the backend to a CSV file.
     *
     * @param file the path where the CSV will be stored.
     * @throws IOException
     */
    public void exportCsv(Path file) throws IOException {
        IFileExport exporter = new FastCsvExporter(source);
        exporter.write(file);
    }
}
