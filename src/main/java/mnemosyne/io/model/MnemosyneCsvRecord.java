package mnemosyne.io.model;

import mnemosyne.domain.Card;

/**
 * Tuple of {@link Card} and {@link mnemosyne.domain.Category} for use with reading and writing CSV data.
 */
public class MnemosyneCsvRecord {
    public FlatCategory category;
    public Card card;

    public MnemosyneCsvRecord() {
    }

    public MnemosyneCsvRecord(String parentName, String categoryName, String owner, Card card) {
        this.category = new FlatCategory(parentName, categoryName, owner);
        this.card = card;
    }

    @Override
    public String toString() {
        return "MnemosyneCsvRecord{" +
                "category=" + category +
                ", card=" + card +
                '}';
    }
}
