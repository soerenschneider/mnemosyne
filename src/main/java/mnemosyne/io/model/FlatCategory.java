package mnemosyne.io.model;

import java.util.Objects;

/**
 * Flattened representation of a {@link mnemosyne.domain.Category}.
 */
public class FlatCategory {
    public String parentName;
    public String categoryName;
    public String owner;

    public FlatCategory() {}

    public FlatCategory(String parentName, String categoryName, String owner) {
        this.parentName = parentName;
        this.categoryName = categoryName;
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "FlatCategory{" +
                "parentName='" + parentName + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", owner='" + owner + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlatCategory that = (FlatCategory) o;
        return Objects.equals(parentName, that.parentName) &&
                Objects.equals(categoryName, that.categoryName) &&
                Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {

        return Objects.hash(parentName, categoryName, owner);
    }
}
