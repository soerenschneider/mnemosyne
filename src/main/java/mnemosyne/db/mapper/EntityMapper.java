package mnemosyne.db.mapper;

import lombok.NonNull;
import ma.glasnost.orika.Converter;
import ma.glasnost.orika.Mapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Provides spring-aware mapping via orika.
 */
@Component
public class EntityMapper extends ConfigurableMapper implements ApplicationContextAware {
    private MapperFactory factory;
    private ApplicationContext applicationContext;

    public EntityMapper() {
        super(false);
    }

    @Override
    protected void configure(MapperFactory factory) {
        this.factory = factory;
        addAllSpringBeans(applicationContext);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        // Make sure orika is initiated after spring's ready...
        this.applicationContext = applicationContext;
        init();
    }

    /**
     * Adds a mapper to the factory.
     *
     * @param mapper the mapper to add.
     */
    public void addMapper(@NonNull Mapper<?, ?> mapper) {
        factory.classMap(mapper.getAType(), mapper.getBType())
                .byDefault()
                .customize((Mapper) mapper)
                .register();
    }

    /**
     * Adds a converter to the factory.
     *
     * @param converter the converted to add.
     */
    public void addConverter(@NonNull Converter<?, ?> converter) {
        factory.getConverterFactory().registerConverter(converter);
    }

    /**
     * Find all mapper beans using the app context.
     */
    private void addAllSpringBeans(final ApplicationContext applicationContext) {
        Map<String, Mapper> mappers = applicationContext.getBeansOfType(Mapper.class);
        for (Mapper mapper : mappers.values())
        {
            addMapper(mapper);
        }
        Map<String, Converter> converters = applicationContext.getBeansOfType(Converter.class);
        for (Converter converter : converters.values())
        {
            addConverter(converter);
        }
    }
}

