package mnemosyne.db.mapper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.NonNull;

import java.lang.reflect.Type;
import java.util.Set;

public class AnswersJsonSerializer {
    private Gson gson = new Gson();

    private Type setType = new TypeToken<Set<String>>() {}.getType();

    public String toJson(@NonNull Set<String> answers) {
        return gson.toJson(answers);
    }

    public Set<String> fromJson(@NonNull String answers) {
        return gson.fromJson(answers, setType);
    }
}
