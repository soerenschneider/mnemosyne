package mnemosyne.db.mapper;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import mnemosyne.db.entities.CardEntity;
import mnemosyne.domain.Card;
import org.springframework.stereotype.Component;

@Component
public class CardMapper extends CustomMapper<Card, CardEntity> {

    private AnswersJsonSerializer answersSerializer = new AnswersJsonSerializer();

    @Override
    public void mapAtoB(Card card, CardEntity entity, MappingContext context) {
        if (card.getAnswers() != null && !card.getAnswers().isEmpty()) {
            String json = answersSerializer.toJson(card.getAnswers());
            entity.setAnswers(json);
        }
    }

    @Override
    public void mapBtoA(CardEntity entity, Card card, MappingContext context) {
        if (null != entity.getAnswers() && !entity.getAnswers().isEmpty()) {
            card.setAnswers(answersSerializer.fromJson(entity.getAnswers()));
        }
    }
}
