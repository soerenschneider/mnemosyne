package mnemosyne.db.mapper;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import mnemosyne.db.entities.CategoryEntity;
import mnemosyne.domain.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper extends CustomMapper<Category, CategoryEntity> {
    @Override
    public void mapAtoB(Category Category, CategoryEntity entity, MappingContext context) {

    }

    @Override
    public void mapBtoA(CategoryEntity entity, Category Category, MappingContext context) {

    }
}
