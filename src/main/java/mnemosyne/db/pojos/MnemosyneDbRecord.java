package mnemosyne.db.pojos;

import mnemosyne.db.entities.CardEntity;
import mnemosyne.io.model.FlatCategory;

public class MnemosyneDbRecord {
    public FlatCategory category;
    public CardEntity card;

    public MnemosyneDbRecord(String parentName, String categoryName, String owner, CardEntity card) {
        this.category = new FlatCategory(parentName, categoryName, owner);
        this.card = card;
    }
}
