package mnemosyne.db.pojos;

import mnemosyne.db.entities.CategoryEntity;

/**
 * Simple POJO for storing flat category information.
 */
public class FlatCategories {
    public final int category_id;
    public final String category_name;
    public final String parent_category_name;
    public final Integer parent_id;
    public final long count;
    public final boolean isParentCategory;

    public FlatCategories(int category_id, String category_name, String parent_category_name, CategoryEntity parent, long count) {
        this.category_id = category_id;
        this.category_name = category_name;
        this.parent_category_name = parent_category_name;

        if (null != parent) {
            this.parent_id = parent.getId();
        } else {
            this.parent_id = null;
        }

        this.count = count;
        this.isParentCategory = this.parent_id == null;
    }

    public int getCategory_id() {
        return category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public String getParent_category_name() {
        return parent_category_name;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public long getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "FlatCategories{" +
                "category_id=" + category_id +
                ", category_name='" + category_name + '\'' +
                ", parent_category_name='" + parent_category_name + '\'' +
                ", parent_id=" + parent_id +
                ", count=" + count +
                ", isParentCategory=" + isParentCategory +
                '}';
    }
}
