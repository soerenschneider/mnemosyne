package mnemosyne.db.dao.categories;

import mnemosyne.db.entities.CategoryEntity;
import org.springframework.data.repository.CrudRepository;

public interface ICategoriesDao extends CrudRepository<CategoryEntity, Long>, ICustomCategoriesDao {
}