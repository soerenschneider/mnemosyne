package mnemosyne.db.dao.categories;

import mnemosyne.db.entities.CategoryEntity;
import mnemosyne.db.pojos.FlatCategories;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ICustomCategoriesDao {
    @Query("select c from CategoryEntity c where c.id = ?1 and c.owner = ?2")
    Optional<CategoryEntity> findCategory(int id, String owner);

    @Query("select c from CategoryEntity c where c.name = ?1 and c.owner = ?2")
    Optional<CategoryEntity> findCategoryByName(String name, String owner);

    @Query("select c from CategoryEntity c join c.parent as parent where parent.name = ?1 and c.name = ?2 and c.owner = ?3")
    Optional<CategoryEntity> findCategoryByName(String parent, String name, String owner);

    @Query("select c from CategoryEntity c where c.name = ?1 and c.owner = ?2 and c.parent is null")
    Optional<CategoryEntity> findParentByName(String parent, String owner);

    @Query("select c from CategoryEntity c where c.owner = ?1")
    List<CategoryEntity> findByOwner(String owner);

    @Query("select c from CategoryEntity c")
    List<CategoryEntity> findSubcategories(String id);

    @Query("SELECT new mnemosyne.db.pojos.FlatCategories(o.id, o.name as cat_name, i.name as parent_name, o.parent, (select count(card.id) from CardEntity card join CategoryEntity cat on card.category = cat.id where cat.id = o.id)) FROM CategoryEntity o left outer join o.parent as i WHERE o.owner = ?1")
    List<FlatCategories> findCategories(String owner);
}