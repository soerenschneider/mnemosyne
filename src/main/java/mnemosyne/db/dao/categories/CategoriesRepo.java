package mnemosyne.db.dao.categories;

import lombok.NonNull;
import mnemosyne.api.payload.categories.HierachCategories;
import mnemosyne.db.pojos.FlatCategories;
import mnemosyne.db.entities.CategoryEntity;
import mnemosyne.db.mapper.EntityMapper;
import mnemosyne.domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class CategoriesRepo {

    @Autowired
    private EntityMapper mapper;

    private ICategoriesDao dao;

    public CategoriesRepo(@Autowired ICategoriesDao dao) {
        this.dao = dao;
    }

    public boolean isSubcategory(int id, String owner) {
        Optional<CategoryEntity> cat = dao.findCategory(id, owner);
        cat.orElseThrow(() -> new RuntimeException("No such category"));
        return cat.get().getParent() != null;
    }

    public Category saveCategory(@NonNull Category category) {
        CategoryEntity entity = mapper.map(category, CategoryEntity.class);
        CategoryEntity saved = dao.save(entity);
        return mapper.map(saved, Category.class);
    }

    public Optional<Category> findByName(String name, String owner) {
        Optional<CategoryEntity> cat = dao.findCategoryByName(name, owner);
        if (cat.isPresent()) {
            return Optional.of(mapper.map(cat.get(), Category.class));
        }

        return Optional.empty();
    }

    public Category createOrFind(String parent, String name, String owner) {
        Optional<Category> read = findByName(parent, name, owner);
        if (read.isPresent()) {
            return read.get();
        }

        Category parentCategory = findParentByName(parent, owner).orElse(new Category());
        if (parentCategory.getId() == 0) {
            parentCategory.setName(parent);
            parentCategory.setOwner(owner);
            parentCategory = saveCategory(parentCategory);
        }

        Category newCategory = new Category();
        newCategory.setName(name);
        newCategory.setOwner(owner);
        newCategory.setParent(parentCategory);

        return saveCategory(newCategory);
    }

    public Optional<Category> findParentByName(String parent, String owner) {
        Optional<CategoryEntity> cat = dao.findParentByName(parent, owner);
        if (cat.isPresent()) {
            return Optional.of(mapper.map(cat.get(), Category.class));
        }

        return Optional.empty();
    }

    public Optional<Category> findByName(String parent, String name, String owner) {
        Optional<CategoryEntity> cat = dao.findCategoryByName(parent, name, owner);
        if (cat.isPresent()) {
            return Optional.of(mapper.map(cat.get(), Category.class));
        }

        return Optional.empty();
    }

    public Optional<Category> find(int id, String owner) {
        Optional<CategoryEntity> cat = dao.findCategory(id, owner);
        if (cat.isPresent()) {
            return Optional.of(mapper.map(cat.get(), Category.class));
        }

        return Optional.empty();
    }

    public int delete(String id, String owner) {
        return 0;
    }

    public List<Category> findByOwner(String owner) {
        List<CategoryEntity> read = dao.findByOwner(owner);
        List<Category> mapped = new ArrayList<>(read.size());
        read.forEach(x -> mapped.add(mapper.map(x, Category.class)));
        return mapped;
    }

    public HierachCategories getHierarchicalCategories(String owner) {
        final List<FlatCategories> read = dao.findCategories(owner);
        return new HierachCategories(read);
    }

    public List<Category> findSubcategories(String id) {
        List<CategoryEntity> read = dao.findSubcategories(id);

        List<Category> mapped = new ArrayList<>(read.size());
        read.forEach(x -> mapped.add(mapper.map(x, Category.class)));

        return mapped;
    }
}
