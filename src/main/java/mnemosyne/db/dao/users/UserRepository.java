package mnemosyne.db.dao.users;

import mnemosyne.domain.AppUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Provides access to the users known to this system.
 */
@Component
public class UserRepository {

    /** The hard-wired admin user to get things going. */
    @Value("${app.user.admin}")
    private String adminUserName;

    /** The password for the hard-wired admin user. */
    @Value("${app.user.password}")
    private String adminUserPassword;

    /**
     * Finds a user by its unique username.
     *
     * @param username the username to look up
     * @return the found user object or an empty optional, if no such user was found.
     */
    public Optional<AppUser> findByUsername(String username) {
        // Check for the hard-wired user
        if (adminUserName.equalsIgnoreCase(username))
        {
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String hashedPassword = passwordEncoder.encode(adminUserPassword);
            return Optional.ofNullable(new AppUser(adminUserName, hashedPassword));
        }

        // TODO: Connect real repository for user-management

        return Optional.empty();
    }
}
