package mnemosyne.db.dao.cards;

import lombok.extern.slf4j.Slf4j;
import mnemosyne.db.entities.CardEntity;
import mnemosyne.db.mapper.EntityMapper;
import mnemosyne.db.pojos.MnemosyneDbRecord;
import mnemosyne.domain.Card;
import mnemosyne.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class CardsRepo {
    @Autowired
    private EntityMapper mapper;

    @Autowired
    private ICardsDao cardsRepository;

    private int defaultPageSize = 10;

    /**
     * Finds a card based on its id for the currently logged in user.
     *
     * @param id the id of the card to find.
     * @return empty optional if nothing was found (for the current user-id combination) or the object if it was found.
     */
    public Optional<Card> findCard(long id)
    {
        Optional<CardEntity> card = cardsRepository.readCard(getUsername(), id);
        if (!card.isPresent())
        {
            return Optional.empty();
        }
        return Optional.of(mapper.map(card.get(), Card.class));
    }

    public List<MnemosyneDbRecord> dump(int page) {
        PageRequest req = PageRequest.of(page, defaultPageSize);
        return cardsRepository.dump(getUsername(), req);
    }

    private String getUsername() {
        UserPrincipal p = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = p.getUsername();
        return username;
    }

    public long deleteCard(String id) {
        return cardsRepository.delete(getUsername(), id);
    }

    public List<Card> findByCategory(int id)
    {
        PageRequest req = PageRequest.of(0, defaultPageSize, Sort.Direction.ASC, "successfulAnswersRatio", "dateAnsweredLastTime");
        Page<CardEntity> entities = cardsRepository.findBySubCategory(getUsername(), id, req);
        return fromEntity(entities.getContent());
    }

    public Optional<Card> save(Card card)
    {
        CardEntity entity = mapper.map(card, CardEntity.class);
        cardsRepository.save(entity);
        card = mapper.map(entity, Card.class);
        return Optional.of(card);
    }

    public void saveMultiple(List<Card> cards)
    {
        List<CardEntity> entities = new ArrayList<>(cards.size());
        cards.forEach(x -> entities.add(mapper.map(x, CardEntity.class)));
        cardsRepository.saveAll(entities);
    }

    List<Card> fromEntity(List<CardEntity> entities)
    {
        List<Card> cards = new ArrayList<>(entities.size());
        entities.forEach(x -> cards.add(mapper.map(x, Card.class)));
        return cards;
    }
}
