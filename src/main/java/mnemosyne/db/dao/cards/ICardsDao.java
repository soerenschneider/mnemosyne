package mnemosyne.db.dao.cards;

import mnemosyne.db.entities.CardEntity;
import org.springframework.data.repository.CrudRepository;

public interface ICardsDao extends CrudRepository<CardEntity, Long>, ICustomCardsDao
{
}