package mnemosyne.db.dao.cards;

import mnemosyne.db.entities.CardEntity;
import mnemosyne.db.pojos.MnemosyneDbRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Provides queries to the persistence layer that do not come with spring data out of the box.
 */
interface ICustomCardsDao {
    /**
     * Deletes a single document by its owner and id.
     *
     * @param owner the owner of the document to be deleted.
     * @param id the id of the document to be deleted.
     *
     * @return the number of deleted documents.
     */
    @Modifying
    @Transactional
    @Query("delete from CardEntity u where u.id = ?1")
    long delete(String owner, String id);

    /**
     * Reads a card by its owner and id.
     *
     * @param owner the owner of the desired document.
     * @param id the id of the desired document.
     *
     * @return empty optional if no such document could be found, otherwise
     * the found document as {@link CardEntity}.
     */
    @Query("select c from CardEntity c inner join c.category cat where cat.owner = ?1 and c.id = ?2")
    Optional<CardEntity> readCard(String owner, long id);

    /**
     * Finds cards by their owner and appropriate direct category id.
     *
     * @param owner the user who performed the request.
     * @param categoryId the id of the directly associated category.
     * @param pageable pageable instructions.
     *
     * @return cards by their owner and appropriate direct category id.
     */
    @Query("select c from CardEntity c inner join c.category cat where cat.owner = ?1 and c.category = (select icat from CategoryEntity icat where icat.id = ?2)")
    Page<CardEntity> findBySubCategory(String owner, int categoryId, Pageable pageable);

    @Query("select c from CardEntity c join c.category cat on c.category = cat.parent where cat.owner = ?1 and c.category = (select cat from CategoryEntity where cat.id = ?2)")
    Page<CardEntity> findByCategory(String owner, int categoryId, Pageable pageable);

    @Query("select new mnemosyne.db.pojos.MnemosyneDbRecord(parent.name, cat.name, cat.owner, card) from CardEntity card join card.category as cat left outer join cat.parent as parent where cat.owner = ?1")
    List<MnemosyneDbRecord> dump(String owner, Pageable pageable);
}
