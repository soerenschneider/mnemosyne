package mnemosyne.db.entities;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "categories", uniqueConstraints={
        @UniqueConstraint(columnNames = {"parent", "name", "owner"})
})
public class CategoryEntity {
    @Id
    @GeneratedValue
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="parent")
    private CategoryEntity parent;

    @NotEmpty(message = "field must not be empty")
    private String name;

    @NotEmpty(message = "field must not be empty")
    @Column(length=100)
    private String owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CategoryEntity getParent() {
        return parent;
    }

    public void setParent(CategoryEntity parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String
    toString() {
        return "CategoryEntity{" +
                "id='" + id + '\'' +
                ", parent=" + parent +
                ", name='" + name + '\'' +
                ", owner='" + owner + '\'' +
                '}';
    }
}
