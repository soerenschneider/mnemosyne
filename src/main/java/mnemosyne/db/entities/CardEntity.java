package mnemosyne.db.entities;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "cards", uniqueConstraints={
        @UniqueConstraint(columnNames = {"prompt", "answers", "category"})
})
public class CardEntity {
    @Id
    @GeneratedValue
    private long id;

    @NotEmpty(message = "field must not be empty")
    private String prompt;

    @NotEmpty(message = "field must not be empty")
    @Column(length=300)
    private String answers;

    @Column(name="example", columnDefinition="TEXT")
    private String example;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="category")
    private CategoryEntity category;

    @ManyToOne
    @JoinColumn(name="subcategory")
    private CategoryEntity subcategory;

    // Stats
    private int timesAnswered;
    private int timesAnsweredCorrectly;

    private float successfulAnswersRatio;

    // Dates
    private LocalDateTime dateAnsweredLastTime;
    private LocalDateTime dateCreated;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public int getTimesAnswered() {
        return timesAnswered;
    }

    public void setTimesAnswered(int timesAnswered) {
        this.timesAnswered = timesAnswered;
    }

    public int getTimesAnsweredCorrectly() {
        return timesAnsweredCorrectly;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public CategoryEntity getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(CategoryEntity subcategory) {
        this.subcategory = subcategory;
    }

    public void setTimesAnsweredCorrectly(int timesAnsweredCorrectly) {
        this.timesAnsweredCorrectly = timesAnsweredCorrectly;
    }

    public float getSuccessfulAnswersRatio() {
        return successfulAnswersRatio;
    }

    public void setSuccessfulAnswersRatio(float successfulAnswersRatio) {
        this.successfulAnswersRatio = successfulAnswersRatio;
    }

    public LocalDateTime getDateAnsweredLastTime() {
        return dateAnsweredLastTime;
    }

    public void setDateAnsweredLastTime(LocalDateTime dateAnsweredLastTime) {
        this.dateAnsweredLastTime = dateAnsweredLastTime;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }
}
