package mnemosyne.commands;

/**
 * Executes the request that is modeled with an {@link ICommand}.
 * @param <T> the class of the request, which must extend {@link ICommand}.
 * @param <U> the class of the outcome of the operation which must extend {@link CommandOutcome}.
 */
public interface ICommandExecutor<T extends ICommand, U extends CommandOutcome>
{
    /**
     * The returned field is used to tie together an {@link ICommand} and an {@link ICommandExecutor}.
     * Both implementations need to return the same value in order to be tied together.
     *
     * @return the enum field that described this executor.
     */
    Command getCommand();

    /**
     * Performs the logic of the requested command.
     *
     * @param cmd the information/instructions of the command.
     * @return the outcome of this command execution.
     */
    U execute(T cmd);
}
