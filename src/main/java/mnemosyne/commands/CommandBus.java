package mnemosyne.commands;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Executes commands and returns their result to the caller.
 */
@Component
@Slf4j
public class CommandBus {

    @Autowired
    List<ICommandExecutor> executors;

    public CommandOutcome acceptCommand(ICommand cmd) {
        for (ICommandExecutor executor : executors)
        {
            if (cmd.getCommand() == executor.getCommand())
            {
                log.trace("Executing [{}] with executor [{}]", cmd, executor);
                return executor.execute(cmd);
            }
        }

        log.warn("No command handler found for [{}]", cmd.getCommand());
        return null;
    }
}