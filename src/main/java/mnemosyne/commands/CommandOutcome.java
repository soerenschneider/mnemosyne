package mnemosyne.commands;

/**
 * Describes the basic outcome of a command.
 */
public interface CommandOutcome {
}
