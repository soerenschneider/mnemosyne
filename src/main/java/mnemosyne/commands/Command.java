package mnemosyne.commands;

/**
 * Works as inventory for all available commands.
 */
public enum Command {
    CARDS_CREATE,
    CARDS_DELETE,
    CARDS_GUESS,
    CARDS_IMPORT,
    CARDS_HINT,
    TAGS_LOAD,
    CATEGORY_CREATE,
    CATEGORY_DELETE;
}
