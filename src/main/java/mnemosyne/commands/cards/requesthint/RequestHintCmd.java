package mnemosyne.commands.cards.requesthint;

import mnemosyne.commands.Command;
import mnemosyne.commands.ICommand;

public class RequestHintCmd implements ICommand {
    @Override
    public Command getCommand() {
        return Command.CARDS_HINT;
    }
}
