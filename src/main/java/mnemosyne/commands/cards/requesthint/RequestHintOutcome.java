package mnemosyne.commands.cards.requesthint;

import lombok.Builder;
import mnemosyne.commands.CommandOutcome;

@Builder
public class RequestHintOutcome implements CommandOutcome {
    public final String hint;
    public final Boolean ok;
    public final String message;
}
