package mnemosyne.commands.cards.requesthint;

import lombok.extern.slf4j.Slf4j;
import mnemosyne.cache.Session;
import mnemosyne.cache.SessionRepo;
import mnemosyne.commands.Command;
import mnemosyne.commands.ICommandExecutor;
import mnemosyne.db.dao.cards.CardsRepo;
import mnemosyne.domain.Card;
import mnemosyne.game.hints.IHintGenerator;
import mnemosyne.security.UserPrincipal;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class RequestHintExecutor implements ICommandExecutor<RequestHintCmd, RequestHintOutcome> {
    @Autowired
    private IHintGenerator hintGenerator;

    @Autowired
    private CardsRepo cardsRepo;

    @Autowired
    private SessionRepo sessionRepo;

    @Override
    public Command getCommand() {
        return Command.CARDS_HINT;
    }

    @Override
    public RequestHintOutcome execute(RequestHintCmd cmd) {
        Optional<Session> session = sessionRepo.findById(getUsername());
        if (!session.isPresent()) {
            return RequestHintOutcome.builder().message("No session").ok(false).build();
        }

        Long currentId = session.get().getCurrentCardId();
        if (null != currentId) {
            Optional<Card> card = cardsRepo.findCard(currentId);
            if (card.isPresent()) {
                String hint = hintGenerator.generateHint(card.get());
                return RequestHintOutcome.builder().hint(hint).build();
            }
            log.warn("Could not read card [{}] for user [{}]", currentId, getUsername());
        }

        return RequestHintOutcome.builder().message("Could not read card").ok(false).build();
    }

    private String getUsername() {
        UserPrincipal p = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = p.getUsername();
        return username;
    }
}
