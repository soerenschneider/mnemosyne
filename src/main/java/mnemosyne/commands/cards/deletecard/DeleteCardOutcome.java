package mnemosyne.commands.cards.deletecard;

import lombok.Builder;
import mnemosyne.commands.CommandOutcome;

@Builder
public class DeleteCardOutcome implements CommandOutcome {
    public final Long deleted;
    public final Boolean ok;
    public final String message;
}
