package mnemosyne.commands.cards.deletecard;

import mnemosyne.commands.Command;
import mnemosyne.commands.ICommandExecutor;
import mnemosyne.db.dao.cards.CardsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeleteCardExecutor implements ICommandExecutor<DeleteCardCmd, DeleteCardOutcome> {

    @Autowired
    private CardsRepo cardsRepo;

    @Override
    public Command getCommand() {
        return Command.CARDS_DELETE;
    }

    @Override
    public DeleteCardOutcome execute(DeleteCardCmd cmd) {
        long amountDeleted = cardsRepo.deleteCard(cmd.id);
        if (amountDeleted > 0)
        {
            return DeleteCardOutcome.builder().ok(true).message(String.format("Deleted %d entries", amountDeleted)).deleted(amountDeleted).build();
        }

        return DeleteCardOutcome.builder().ok(false).message("Could not delete object. ").deleted(amountDeleted).build();
    }
}
