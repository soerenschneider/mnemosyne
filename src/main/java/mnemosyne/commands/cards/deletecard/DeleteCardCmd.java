package mnemosyne.commands.cards.deletecard;

import mnemosyne.commands.Command;
import mnemosyne.commands.ICommand;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class DeleteCardCmd implements ICommand {
    @NotBlank
    public String id;

    @Override
    public Command getCommand() {
        return Command.CARDS_DELETE;
    }
}
