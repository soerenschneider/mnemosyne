package mnemosyne.commands.cards.createcard;

import mnemosyne.domain.Card;
import mnemosyne.commands.Command;
import mnemosyne.commands.ICommand;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.HashSet;

public class CreateCardCmd implements ICommand {
    @NotBlank
    public String prompt;

    @NotNull
    public String[] answers;

    public String example;

    @NotNull
    public int categoryId;

    public Card toCard() {
        Card card = new Card();

        card.setAnswers(new HashSet<>(Arrays.asList(answers)));

        card.setPrompt(prompt);
        card.setExample(example);

        return card;
    }

    @Override
    public Command getCommand() {
        return Command.CARDS_CREATE;
    }
}
