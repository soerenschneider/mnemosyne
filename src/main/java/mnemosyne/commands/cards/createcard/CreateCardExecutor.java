package mnemosyne.commands.cards.createcard;

import mnemosyne.commands.Command;
import mnemosyne.commands.ICommandExecutor;
import mnemosyne.db.dao.cards.CardsRepo;
import mnemosyne.db.dao.categories.CategoriesRepo;
import mnemosyne.domain.Card;
import mnemosyne.domain.Category;
import mnemosyne.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CreateCardExecutor implements ICommandExecutor<CreateCardCmd, CreateCardOutcome> {

    @Autowired
    private CardsRepo cardsRepo;

    @Autowired
    private CategoriesRepo categoriesRepo;

    @Override
    public Command getCommand() {
        return Command.CARDS_CREATE;
    }

    @Override
    public CreateCardOutcome execute(CreateCardCmd cmd) {
        Optional<Category> category = categoriesRepo.find(cmd.categoryId, getUsername());
        if (! category.isPresent()) {
            throw new IllegalArgumentException("No such category");
        }

        Card card = cmd.toCard();
        card.setCategory(category.get());

        Optional<Card> saved = cardsRepo.save(card);
        if (saved.isPresent())
        {
            return CreateCardOutcome.builder().ok(true).message("Saved successfully").id(saved.get().getId()+"").build();
        }

        return CreateCardOutcome.builder().ok(false).message("Could not save object").build();
    }

    private String getUsername() {
        UserPrincipal p = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return p.getUsername();
    }
}
