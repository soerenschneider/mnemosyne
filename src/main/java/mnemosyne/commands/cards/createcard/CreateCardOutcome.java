package mnemosyne.commands.cards.createcard;

import lombok.Builder;
import mnemosyne.commands.CommandOutcome;

@Builder
public class CreateCardOutcome implements CommandOutcome {
    public final String id;
    public final Boolean ok;
    public final String message;
}