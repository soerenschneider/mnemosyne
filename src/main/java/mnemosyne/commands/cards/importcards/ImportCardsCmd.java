package mnemosyne.commands.cards.importcards;

import mnemosyne.commands.Command;
import mnemosyne.commands.ICommand;
import mnemosyne.domain.Card;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.File;
import java.util.Arrays;
import java.util.List;

public class ImportCardsCmd implements ICommand {
    public List<Card> cards;

    @Override
    public Command getCommand() {
        return Command.CARDS_IMPORT;
    }
}
