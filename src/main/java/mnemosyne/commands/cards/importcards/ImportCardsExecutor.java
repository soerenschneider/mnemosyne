package mnemosyne.commands.cards.importcards;

import mnemosyne.cache.CardsSequence;
import mnemosyne.cache.Session;
import mnemosyne.cache.SessionRepo;
import mnemosyne.commands.Command;
import mnemosyne.commands.ICommandExecutor;
import mnemosyne.db.dao.cards.CardsRepo;
import mnemosyne.domain.Card;
import mnemosyne.evaluator.AnswerEvaluation;
import mnemosyne.evaluator.IAnswerEvaluator;
import mnemosyne.security.UserPrincipal;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ImportCardsExecutor implements ICommandExecutor<ImportCardsCmd, ImportCardsOutcome> {
    @Autowired
    private IAnswerEvaluator evaluator;

    @Autowired
    private CardsSequence sequence;

    @Autowired
    private CardsRepo cardsRepo;

    @Autowired
    private SessionRepo sessionRepo;

    @Override
    public Command getCommand() {
        return Command.CARDS_IMPORT;
    }

    @Override
    public ImportCardsOutcome execute(ImportCardsCmd cmd) {
        cardsRepo.saveMultiple(cmd.cards);
        return ImportCardsOutcome.builder().ok(true).build();
    }

}
