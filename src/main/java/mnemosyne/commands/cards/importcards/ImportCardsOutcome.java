package mnemosyne.commands.cards.importcards;

import lombok.Builder;
import mnemosyne.commands.CommandOutcome;
import mnemosyne.commands.GenericCommandOutcome;

import java.util.Set;

@Builder
public class ImportCardsOutcome implements CommandOutcome {
    public final String message;
    public final Boolean ok;
}
