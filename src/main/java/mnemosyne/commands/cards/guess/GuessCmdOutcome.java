package mnemosyne.commands.cards.guess;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Singular;
import mnemosyne.commands.CommandOutcome;
import mnemosyne.domain.Card;
import mnemosyne.evaluator.AnswerEvaluation;

import java.util.Collections;
import java.util.Set;

@Builder
public class GuessCmdOutcome implements CommandOutcome {
    public final String guess;
    public final Set<String> actual;
    public final String description;
    public final Boolean accepted;
    public final String message;

    @JsonProperty("game_over")
    public final Boolean gameOver;

    @JsonProperty("eval_result")
    public final AnswerEvaluation evaluationResult;

    public final Boolean ok;
}
