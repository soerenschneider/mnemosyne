package mnemosyne.commands.cards.guess;

import lombok.extern.slf4j.Slf4j;
import mnemosyne.cache.CardsSequence;
import mnemosyne.domain.Card;
import mnemosyne.cache.Session;
import mnemosyne.cache.SessionRepo;
import mnemosyne.commands.Command;
import mnemosyne.commands.ICommandExecutor;
import mnemosyne.db.dao.cards.CardsRepo;
import mnemosyne.evaluator.AnswerEvaluation;
import mnemosyne.evaluator.IAnswerEvaluator;
import mnemosyne.security.UserPrincipal;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class GuessExecutor implements ICommandExecutor<GuessCmd, GuessCmdOutcome> {

    @Autowired
    private IAnswerEvaluator evaluator;

    @Autowired
    private CardsSequence sequence;

    @Autowired
    private CardsRepo cardsRepo;

    @Autowired
    private SessionRepo sessionRepo;

    @Override
    public Command getCommand() {
        return Command.CARDS_GUESS;
    }

    @Override
    public GuessCmdOutcome execute(GuessCmd cmd) {
        Optional<Session> session = sessionRepo.findById(getUsername());
        if (! session.isPresent())
        {
            return GuessCmdOutcome.builder().ok(false).message("You have no session").build();
        }

        if (! cmd.id.equals(session.get().getCurrentCardId()))
        {
            if (null == session.get().getCurrentCardId()) {
                return GuessCmdOutcome.builder().ok(false).message("Game over").gameOver(true).build();
            }

            return GuessCmdOutcome.builder().ok(false).message("Your card (" + cmd.id + ") does not match the session's card: " + session.get().getCurrentCardId()).build();
        }

        log.debug("User [{}] requested a guess for [{}]", getUsername(), cmd.id);

        Optional<Card> read = cardsRepo.findCard(cmd.id);
        if (!read.isPresent())
        {
            log.debug("No card found for user [{}] and id [{}]", getUsername(), cmd.id);
            return GuessCmdOutcome.builder().ok(false).message("No card found for id: " + cmd.id).build();
        }

        final AnswerEvaluation evaluation = evaluator.guess(cmd.guess, read.get());
        cardsRepo.save(read.get());

        // Set null as next id if there is nothing else available
        Long nextId = sequence.getNextCardId(getUsername()).orElse(null);
        session.get().incGuesses(evaluation != AnswerEvaluation.UNACCEPTABLE_GUESS);
        session.get().setCurrentCardId(nextId);
        sessionRepo.save(session.get());

        log.debug("Guess from user [{}] for prompt [{}] was [{}]", getUsername(), read.get().getPrompt(), evaluation.toString());
        return buildResponseFromCard(read.get(), evaluation).build();
    }

    private GuessCmdOutcome.GuessCmdOutcomeBuilder buildResponseFromCard(Card card, AnswerEvaluation eval) {
        final boolean badGuess = AnswerEvaluation.UNACCEPTABLE_GUESS == eval;

        GuessCmdOutcome.GuessCmdOutcomeBuilder builder = GuessCmdOutcome.builder().description(card.getExample()).actual(card.getAnswers()).evaluationResult(eval).accepted(!badGuess);
        return builder;
    }

    private String getUsername() {
        UserPrincipal p = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return p.getUsername();
    }
}
