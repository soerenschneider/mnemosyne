package mnemosyne.commands.cards.guess;

import mnemosyne.commands.Command;
import mnemosyne.commands.ICommand;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class GuessCmd implements ICommand {
    @NotNull
    public Long id;

    @NotBlank
    public String guess;

    @Override
    public Command getCommand() {
        return Command.CARDS_GUESS;
    }

    @Override
    public String toString() {
        return "GuessCmd{" +
                ", id=" + id +
                ", guess='" + guess + '\'' +
                '}';
    }
}
