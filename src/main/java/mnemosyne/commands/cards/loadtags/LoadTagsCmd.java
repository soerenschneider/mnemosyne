package mnemosyne.commands.cards.loadtags;


import mnemosyne.commands.Command;
import mnemosyne.commands.ICommand;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class LoadTagsCmd implements ICommand {
    @NotNull
    public int category;

    @Override
    public Command getCommand() {
        return Command.TAGS_LOAD;
    }
}
