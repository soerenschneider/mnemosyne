package mnemosyne.commands.cards.loadtags;

import mnemosyne.cache.CardsSequence;
import mnemosyne.commands.CommandOutcome;
import mnemosyne.commands.GenericCommandOutcome;
import mnemosyne.domain.Card;
import mnemosyne.cache.Session;
import mnemosyne.cache.SessionRepo;
import mnemosyne.commands.Command;
import mnemosyne.commands.ICommandExecutor;
import mnemosyne.db.dao.cards.CardsRepo;
import lombok.extern.slf4j.Slf4j;
import mnemosyne.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class LoadTagsExecutor implements ICommandExecutor<LoadTagsCmd, CommandOutcome> {
    @Autowired
    private CardsSequence sequenceRepo;

    @Autowired
    private SessionRepo sessionRepo;

    @Autowired
    private CardsRepo cardsRepo;

    @Override
    public Command getCommand() {
        return Command.TAGS_LOAD;
    }

    @Override
    public CommandOutcome execute(LoadTagsCmd cmd) {
        List<Long> ids = cardsRepo.findByCategory(cmd.category).stream().map(x -> x.getId()).collect(Collectors.toList());

        log.debug("Loaded {} cards for id {}", ids.size(), cmd.category);
        if (ids.isEmpty())
        {
            return GenericCommandOutcome.builder().ok(false).message("Nothing found for id: " + cmd.category).build();
        }

        // Reset list of previously loaded ids and push new ids...
        sequenceRepo.reset(getUsername());
        sequenceRepo.addCardIds(getUsername(), ids);

        Session session = sessionRepo.findById(getUsername()).orElse(new Session(getUsername()));
        session.reset();

        // pop the first id we just wrote
        session.setCurrentCardId(sequenceRepo.getNextCardId(getUsername()).orElse(null));
        session.setCurrentCategoryId(cmd.category);
        sessionRepo.save(session);

        return GenericCommandOutcome.builder().ok(true).message(String.format("Loaded %d entries", ids.size())).build();
    }

    private String getUsername() {
        UserPrincipal p = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return p.getUsername();
    }
}
