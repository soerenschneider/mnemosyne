package mnemosyne.commands;

/**
 * Described a request for an action.
 */
public interface ICommand
{
    /**
     * The returned field is used to tie together an {@link ICommand} and an {@link ICommandExecutor}.
     * Both implementations need to return the same value in order to be tied together.
     *
     * @return the enum field that described this command.
     */
    Command getCommand();
}
