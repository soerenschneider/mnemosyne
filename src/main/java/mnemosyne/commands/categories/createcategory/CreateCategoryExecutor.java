package mnemosyne.commands.categories.createcategory;

import mnemosyne.commands.Command;
import mnemosyne.commands.ICommandExecutor;
import mnemosyne.db.dao.categories.CategoriesRepo;
import mnemosyne.domain.Category;
import mnemosyne.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CreateCategoryExecutor implements ICommandExecutor<CreateCategoryCmd, CreateCategoryOutcome> {

    @Autowired
    private CategoriesRepo categoriesRepo;

    @Override
    public Command getCommand() {
        return Command.CATEGORY_CREATE;
    }

    @Override
    public CreateCategoryOutcome execute(CreateCategoryCmd cmd) {
        Category cat = buildCategoryFromCmd(cmd);
        Category saved = categoriesRepo.saveCategory(cat);

        CreateCategoryOutcome outcome = new CreateCategoryOutcome();
        outcome.category = saved;
        return outcome;
    }

    private Category buildCategoryFromCmd(CreateCategoryCmd cmd) {
        Category cat = new Category();
        cat.setName(cmd.name);
        cat.setOwner(getUsername());

        if (null == cmd.parentId) {
            // Avoid stupid MySQL regarding constraints on fields with a NULL value
            Optional<Category> read = categoriesRepo.findByName(cmd.name, getUsername());
            if (read.isPresent()) {
                throw new IllegalArgumentException("Category '" + cmd.name + "' already existing with id: " + read.get().getId());
            }

            return cat;
        }

        // Read category just to add the reference, loving JPA.
        Optional<Category> parent = categoriesRepo.find(cmd.parentId, getUsername());
        if (parent.isPresent()) {
            if (null != parent.get().getParent()) {
                throw new IllegalArgumentException("No further hiearchy allowed");
            }
            cat.setParent(parent.get());
        } else {
            throw new IllegalArgumentException("No such parent id");
        }

        return cat;
    }

    private String getUsername() {
        UserPrincipal p = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return p.getUsername();
    }
}
