package mnemosyne.commands.categories.createcategory;

import mnemosyne.commands.Command;
import mnemosyne.commands.ICommand;
import mnemosyne.domain.validators.CategoryNameConstraint;

public class CreateCategoryCmd implements ICommand {
    @CategoryNameConstraint
    public String name;

    public Integer parentId;

    @Override
    public Command getCommand() {
        return Command.CATEGORY_CREATE;
    }
}
