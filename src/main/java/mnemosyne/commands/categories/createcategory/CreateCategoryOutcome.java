package mnemosyne.commands.categories.createcategory;

import mnemosyne.commands.CommandOutcome;
import mnemosyne.domain.Category;

public class CreateCategoryOutcome implements CommandOutcome {
    public Category category;
}

