package mnemosyne.commands.categories.deletecategory;

import mnemosyne.commands.Command;
import mnemosyne.commands.ICommandExecutor;
import mnemosyne.db.dao.categories.CategoriesRepo;
import mnemosyne.domain.Category;
import mnemosyne.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DeleteCategoryExecutor implements ICommandExecutor<DeleteCategoryCmd, DeleteCategoryOutcome> {

    @Autowired
    private CategoriesRepo categoriesRepo;

    @Override
    public Command getCommand() {
        return Command.CATEGORY_DELETE;
    }

    @Override
    public DeleteCategoryOutcome execute(DeleteCategoryCmd cmd) {
        categoriesRepo.delete(cmd.id, getUsername());
        DeleteCategoryOutcome outcome = new DeleteCategoryOutcome();
        return outcome;
    }

    private String getUsername() {
        UserPrincipal p = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return p.getUsername();
    }
}
