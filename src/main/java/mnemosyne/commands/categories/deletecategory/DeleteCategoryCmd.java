package mnemosyne.commands.categories.deletecategory;

import mnemosyne.commands.Command;
import mnemosyne.commands.ICommand;

import javax.validation.constraints.NotBlank;

public class DeleteCategoryCmd implements ICommand {
    public DeleteCategoryCmd() {}

    public DeleteCategoryCmd(String id) {
        this.id = id;
    }

    @NotBlank
    public String id;

    @Override
    public Command getCommand() {
        return Command.CATEGORY_DELETE;
    }
}
