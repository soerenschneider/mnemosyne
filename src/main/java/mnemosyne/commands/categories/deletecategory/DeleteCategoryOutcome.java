package mnemosyne.commands.categories.deletecategory;

import mnemosyne.commands.CommandOutcome;
import mnemosyne.domain.Category;

public class DeleteCategoryOutcome implements CommandOutcome {
    public boolean ok;
    public int deletedDocuments;
}

