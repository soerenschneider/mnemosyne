package mnemosyne.commands;

import lombok.Builder;

@Builder
public class GenericCommandOutcome implements CommandOutcome {
    public final String message;
    public final Boolean ok;
}
