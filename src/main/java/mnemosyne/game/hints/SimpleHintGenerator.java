package mnemosyne.game.hints;

import lombok.NonNull;
import mnemosyne.domain.Card;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class SimpleHintGenerator implements IHintGenerator {

    private Random rand = new java.util.Random();

    /** The character to perform the obfuscation with. */
    private final char obfuscationLetter = '*';

    /** The ratio of letters to obfuscate. */
    private final int ratio = 3;

    @Override
    public String generateHint(@NonNull Card card) {
        char[] answer = String.join(", ", card.getAnswers()).toCharArray();

        List<Integer> obfuscatableIndices = getIndexCandidates(answer);

        // Calculate the amount of obfuscated letters on letter characters.
        int amount = obfuscatableIndices.size() / ratio;
        for (int i = 0; i < amount; i++) {
            int rem = rand.nextInt(obfuscatableIndices.size());
            obfuscatableIndices.remove(Integer.valueOf(rem));
            answer[obfuscatableIndices.get(i)] = obfuscationLetter;
        }

        return new String(answer);
    }

    /**
     * Returns the indices of the char array that can be used for obfuscation. If a character in the array
     * is a non-letter character, the letter is not used for obfuscation.
     *
     * @param word the word to build the result for.
     * @return a list containing the indices of characters that are letters.
     */
    private List<Integer> getIndexCandidates(char[] word) {
        List<Integer> indices = IntStream.range(0, word.length).boxed().collect(Collectors.toList());
        for (int index = 0; index < word.length; index++) {
            char c = word[index];
            if (c == ' ') {
                indices.remove(Integer.valueOf(index));
            }
        }

        return indices;
    }
}
