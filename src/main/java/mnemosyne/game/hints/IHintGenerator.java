package mnemosyne.game.hints;

import mnemosyne.domain.Card;

/**
 * Generates a hint for the clueless user for a given card.
 */
public interface IHintGenerator {
    /**
     * Generates a hint in a form of an extracted answer from the card but with randomly distributed, 'blurred'
     * characters '*' instead of the real characters.
     *
     * @param card the card to generate a hint for.
     * @return an answer where the rules have been applied to.
     */
    String generateHint(Card card);
}
