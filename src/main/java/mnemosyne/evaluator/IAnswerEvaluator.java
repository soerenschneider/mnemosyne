package mnemosyne.evaluator;

import mnemosyne.domain.Card;
import lombok.NonNull;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * Provides a specific mechanism that evaluates user input.
 */
public interface IAnswerEvaluator {
    /**
     * Checks whether the given answer for a card by the user is acceptable.
     *
     * @param givenAnswer the answer provided by the user.
     * @param acceptableAnswers a set of acceptable answers.
     *
     * @return AnswerEvaluation.CORRECT_GUESS, if the answer is accepted as 'correct' by the system,
     * AnswerEvaluation.UNACCEPTABLE_GUESS otherwise.
     */
    AnswerEvaluation matches(String givenAnswer, Set<String> acceptableAnswers);

    /**
     * Checks the given answer with the appropriate strategy and updates the
     * the supplied card object.
     *
     * @param givenAnswer the answer provided by the user.
     * @param card the card to test the answer against.
     *
     * @return AnswerEvaluation.CORRECT_GUESS, if the answer is accepted as 'correct' by the system,
     * AnswerEvaluation.UNACCEPTABLE_GUESS otherwise.
     */
    default AnswerEvaluation guess(@NonNull String givenAnswer, @NonNull Card card) {
        AnswerEvaluation eval = matches(givenAnswer, card.getAnswers());
        applyEvaluationResults(card, eval);
        return eval;
    }

    /**
     * Updates the card object with regard of the supplied evaluation result.
     *
     * @param card the card to apply the results to.
     * @param eval the evaluation result.
     */
    default void applyEvaluationResults(Card card, AnswerEvaluation eval) {
        card.setDateAnsweredLastTime(LocalDateTime.now());
        card.setTimesAnswered(1 + card.getTimesAnswered());

        if (AnswerEvaluation.UNACCEPTABLE_GUESS != eval) {
            card.setTimesAnsweredCorrectly(1 + card.getTimesAnsweredCorrectly());
        }
    }
}
