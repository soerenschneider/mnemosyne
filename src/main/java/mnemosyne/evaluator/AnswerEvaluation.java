package mnemosyne.evaluator;

/**
 * Possible states of a answer evaluation operation.
 */
public enum AnswerEvaluation {
    /** Indicates a correct guess. */
    CORRECT_GUESS,

    /** Indicates a guess that does not lie within the acceptable error margin. */
    UNACCEPTABLE_GUESS,

    /** Indicates a guess that's not a 100% correct but lies within the acceptable error margin. */
    ACCEPTABLE_GUESS
}
