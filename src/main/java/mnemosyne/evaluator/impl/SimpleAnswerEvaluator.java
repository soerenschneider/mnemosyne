package mnemosyne.evaluator.impl;

import mnemosyne.evaluator.AnswerEvaluation;
import mnemosyne.evaluator.IAnswerEvaluator;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Naive implementation to get things going.
 */
@Component
public class SimpleAnswerEvaluator implements IAnswerEvaluator {
    @Override
    public AnswerEvaluation matches(String givenAnswer, Set<String> acceptableAnswers) {
        for (String acceptedAnswer : acceptableAnswers)
        {
            if (acceptedAnswer.equalsIgnoreCase(givenAnswer))
            {
                return AnswerEvaluation.CORRECT_GUESS;
            }
        }

        return AnswerEvaluation.UNACCEPTABLE_GUESS;
    }
}
