package mnemosyne.evaluator.impl;

import mnemosyne.evaluator.AnswerEvaluation;
import mnemosyne.evaluator.IAnswerEvaluator;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * A slightly more sophisticated answer evaluator.
 */
@Primary
@Component
public class LevAnswerEvaluator implements IAnswerEvaluator {
    private int grace;
    private final LevenshteinDistance levenshteinImpl;

    public LevAnswerEvaluator() {
        this(3);
    }

    public LevAnswerEvaluator(int grace) {
        this.grace = grace;
        this.levenshteinImpl = new LevenshteinDistance(grace + 1);
    }

    @Override
    public AnswerEvaluation matches(String givenAnswer, Set<String> acceptableAnswers) {
        int minDistance = Integer.MAX_VALUE;

        for (String answer : acceptableAnswers)
        {
            final int distance = levenshteinImpl.apply(givenAnswer, answer);
            // Returned value can be -1 if the calculated distance is exceeding the threshold
            if (distance < minDistance && distance >= 0) {
                minDistance = distance;
            }
        }

        if (0 == minDistance) {
            return AnswerEvaluation.CORRECT_GUESS;
        } else if (0 <= minDistance && minDistance <= grace) {
            return AnswerEvaluation.ACCEPTABLE_GUESS;
        }

        return AnswerEvaluation.UNACCEPTABLE_GUESS;
    }
}
