package mnemosyne.cache;

import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.time.LocalDateTime;

@RedisHash("session")
public class Session {
    @Id
    private String user;

    private Long currentCardId;

    private Integer currentCategoryId;

    private int guesses;
    private int correctGuesses;

    private int totalGuesses;
    private int totalCorrectGuesses;

    public Session(@NonNull String user)
    {
        this.user = user;
    }

    public void reset() {
        this.currentCardId = null;
        this.currentCategoryId = null;

        this.guesses = 0;
        this.correctGuesses = 0;
    }

    public float getCorrectGuessesRatio() {
        if (guesses == 0)
        {
            return 0f;
        }

        return correctGuesses * 100f / guesses;
    }

    public float getTotalCorrectGuessesRatio() {
        if (totalGuesses == 0)
        {
            return 0f;
        }

        return totalCorrectGuesses * 100f / totalGuesses;
    }

    public void incGuesses(boolean correct) {
        this.totalGuesses += 1;
        this.guesses += 1;

        if (correct) {
            this.correctGuesses += 1;
            this.totalCorrectGuesses += 1;
        }
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getCurrentCardId() {
        return currentCardId;
    }

    public void setCurrentCardId(Long currentCardId) {
        this.currentCardId = currentCardId;
    }

    public Integer getCurrentCategoryId() {
        return currentCategoryId;
    }

    public void setCurrentCategoryId(Integer currentCategoryId) {
        this.currentCategoryId = currentCategoryId;
    }

    public int getGuesses() {
        return guesses;
    }

    public void setGuesses(int guesses) {
        this.guesses = guesses;
    }

    public int getCorrectGuesses() {
        return correctGuesses;
    }

    public void setCorrectGuesses(int correctGuesses) {
        this.correctGuesses = correctGuesses;
    }

    public int getTotalGuesses() {
        return totalGuesses;
    }

    public void setTotalGuesses(int totalGuesses) {
        this.totalGuesses = totalGuesses;
    }

    public int getTotalCorrectGuesses() {
        return totalCorrectGuesses;
    }

    public void setTotalCorrectGuesses(int totalCorrectGuesses) {
        this.totalCorrectGuesses = totalCorrectGuesses;
    }
}
