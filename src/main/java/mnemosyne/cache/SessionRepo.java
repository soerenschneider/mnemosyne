package mnemosyne.cache;

import org.springframework.data.repository.CrudRepository;

public interface SessionRepo extends CrudRepository<Session, String> {
}
