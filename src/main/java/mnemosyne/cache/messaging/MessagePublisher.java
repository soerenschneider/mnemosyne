package mnemosyne.cache.messaging;

/**
 * Interface for publishing async messages.
 */
public interface MessagePublisher {
    void publish(String message);
}
