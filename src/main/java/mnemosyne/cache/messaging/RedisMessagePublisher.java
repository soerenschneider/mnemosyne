package mnemosyne.cache.messaging;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;

/**
 * Publish messages via redis.
 */
public class RedisMessagePublisher implements MessagePublisher {
    /** RedisTemplate backend. */
    private RedisTemplate<String, Object> redisTemplate;

    /** The topic to send messages to. */
    private ChannelTopic topic;

    public RedisMessagePublisher(RedisTemplate<String, Object> redisTemplate, ChannelTopic topic) {
        this.redisTemplate = redisTemplate;
        this.topic = topic;
    }

    @Override
    public void publish(String message) {
        redisTemplate.convertAndSend(topic.getTopic(), message);
    }
}
