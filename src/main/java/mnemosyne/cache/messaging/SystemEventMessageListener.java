package mnemosyne.cache.messaging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SystemEventMessageListener implements MessageListener {
    public void onMessage(Message message, byte[] pattern) {
        log.info("Message received: {}", message.toString());
    }
}