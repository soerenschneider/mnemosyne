package mnemosyne.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Holds a list of the next cards to be played for a user. Card ids are represented by redis lists.
 */
@Component
public class CardsSequence {
    @Resource(name="stringRedisTemplate")
    private ListOperations<String, String> listOps;

    /**
     * Returns the id of the next card for the given user.
     *
     * @param user the user that requested the next card.
     * @return the id of the next card for the given user, otherwise an empty optional, if there is no next id.
     */
    public Optional<Long> getNextCardId(String user) {
        final String popped = listOps.leftPop(user);
        if (null == popped) {
            return Optional.empty();
        }
        return Optional.of(Long.valueOf(popped));
    }

    /**
     * Returns the next id but does not remove the element.
     *
     * @param user
     * @return
     */
    public Optional<Long> peek(String user) {
        final String popped = listOps.index(user,0);
        if (null == popped) {
            return Optional.empty();
        }
        return Optional.of(Long.valueOf(popped));
    }

    /**
     * Empties the list of ids for a user.
     * 
     * @param user the user whose list to empty.
     */
    public void reset(String user) {
        listOps.trim(user, -1, 0);
    }

    /**
     * Appends a collection of ids for a user.
     *
     * @param user the user.
     * @param ids the list of ids to append.
     */
    public void addCardIds(String user, Collection<Long> ids) {
        List<String> sids = ids.stream().map(entry -> String.valueOf(entry)).collect(Collectors.toList());
        listOps.rightPushAll(user, sids);
    }

    /**
     * Returns the count of not-yet played cards.
     *
     * @param user the user whose list to query.
     * @return the count of elements for the user's list.
     */
    public long getSize(String user) {
        return listOps.size(user);
    }

    /**
     * Returns whether the list of ids for a given user is empty.
     *
     * @param user the user whose list to test.
     * @return true, if the list of cards is empty for the given user, otherwise false.
     */
    public boolean isEmpty(String user) {
        return 0 == getSize(user);
    }
}
