FROM openjdk:8-jre-alpine
VOLUME /tmp
ARG JAR_FILE
ADD ${JAR_FILE} mnemosyne.jar
ENTRYPOINT ["sh", "-c", "java -Djava.security.egd=file:/dev/./urandom $JAVA_OPTS -jar /mnemosyne.jar"]
