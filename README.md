# mnemosyne

## first interaction

request a json web token:
```
>: curl -H "Content-Type: application/json" -X POST -d '{"username": "admin","password": "secret"}' http://localhost:8080/auth/signin
{"accessToken":"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImlhdCI6MTUyMDk1MzU4NCwiZXhwIjoxNTIxMDM5OTg0fQ.jUm6UWO5lY1-C6SYqBwStMg3YZw7z-d34X6Jz14uU_UyFG7kSYgMcYqdAkfre7-b6W1yH36gZ5MylFukYST96w","tokenType":"Bearer"}
```
export the web token for easier usage:
```
>: export BEARER="eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImlhdCI6MTUyMDk1MzU4NCwiZXhwIjoxNTIxMDM5OTg0fQ.jUm6UWO5lY1-C6SYqBwStMg3YZw7z-d34X6Jz14uU_UyFG7kSYgMcYqdAkfre7-b6W1yH36gZ5MylFukYST96w"
```
use the api for learning your first word in swahili:
```
>: curl -X POST --url 'localhost:8080/cards' -H "Authorization: Bearer $BEARER" -H 'content-type: application/json' --data '{"prompt":"hello","answers":["hujambo"],"tags":["swahili","test"]}'
{"ok":true,"message":"Saved successfully","id":"5aa7e9c85c7c014016d46911"}
```
